(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     26799,        769]
NotebookOptionsPosition[     23504,        702]
NotebookOutlinePosition[     23868,        718]
CellTagsIndexPosition[     23825,        715]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"$TEMPLEPath", "=", 
   RowBox[{"SetDirectory", "[", 
    RowBox[{"StringReplace", "[", 
     RowBox[{
      RowBox[{"NotebookDirectory", "[", "]"}], ",", 
      RowBox[{"\"\<TEMPLE_Examples/\>\"", "\[Rule]", "\"\<TEMPLE5.3\>\""}]}], 
     "]"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"<<", "TEMPLE`"}], ";"}]}], "Input",
 CellChangeTimes->{
  3.785054894445966*^9, {3.853752229051365*^9, 3.853752231481741*^9}, 
   3.8587702814412947`*^9},
 CellLabel->"In[1]:=",ExpressionUUID->"e5fa4d96-81c3-4894-9128-e78798e58b24"],

Cell[CellGroupData[{

Cell[BoxData["\<\"(****** Tools for EMPL Evaluation 0.5.3, Alpha \
******)\"\>"], "Print",
 CellChangeTimes->{3.7854756291685658`*^9, 3.785476575740821*^9, 
  3.7854769934269123`*^9, 3.853752232159634*^9, 3.853752968963966*^9, 
  3.853753131970634*^9, 3.853754020460981*^9, 3.858770282953024*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"3fa3a2ba-0a4d-4378-b2e6-c10887ef436e"],

Cell[BoxData["\<\"(****** Author: Maxim Bezuglov ******)\"\>"], "Print",
 CellChangeTimes->{3.7854756291685658`*^9, 3.785476575740821*^9, 
  3.7854769934269123`*^9, 3.853752232159634*^9, 3.853752968963966*^9, 
  3.853753131970634*^9, 3.853754020460981*^9, 3.8587702829558496`*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"dc527305-85e1-43a8-976b-cc67f46b5908"],

Cell[BoxData["\<\"(****** bezuglov.ma@phystech.edu ******)\"\>"], "Print",
 CellChangeTimes->{3.7854756291685658`*^9, 3.785476575740821*^9, 
  3.7854769934269123`*^9, 3.853752232159634*^9, 3.853752968963966*^9, 
  3.853753131970634*^9, 3.853754020460981*^9, 3.858770282961135*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"c1c14dd4-4966-4392-a0a4-aa497b1d50c5"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FindEquationForRoot", "[", 
  RowBox[{
   RowBox[{"Sqrt", "[", " ", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["\[Eta]", "2"], "+", 
       SuperscriptBox["x", "2"]}], ")"}], 
     RowBox[{"(", 
      RowBox[{
       SuperscriptBox["\[Eta]", "2"], "+", 
       RowBox[{"2", 
        SuperscriptBox["x", "2"]}]}], ")"}]}], "]"}], ",", "y", ",", 
   RowBox[{"{", 
    RowBox[{"\[Eta]", ",", "x"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.785476578367608*^9, 3.78547657976716*^9}, 
   3.785476614216721*^9, {3.853752984294998*^9, 3.8537529849896917`*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"243984b7-19ca-4876-a5a6-b3e416cbf2d5"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", "2"}], " ", 
   SuperscriptBox["x", "4"]}], "+", 
  SuperscriptBox["y", "2"], "-", 
  RowBox[{"3", " ", 
   SuperscriptBox["x", "2"], " ", 
   SuperscriptBox["\[Eta]", "2"]}], "-", 
  SuperscriptBox["\[Eta]", "4"]}]], "Output",
 CellChangeTimes->{
  3.7854766175855494`*^9, 3.785476993631324*^9, 3.853752237242634*^9, {
   3.853752969192203*^9, 3.853752985610306*^9}, 3.853753132193965*^9, 
   3.8537540207415457`*^9, 3.8587702832681713`*^9},
 CellLabel->"Out[3]=",ExpressionUUID->"c0093ab0-9074-4125-9b1f-29984182280e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FindEquationForRoot", "[", 
  RowBox[{
   FractionBox[
    RowBox[{"1", "+", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"x", "+", "1"}], ")"}], 
      FractionBox["1", "2"]], "+", 
     SuperscriptBox["x", 
      FractionBox["1", "3"]]}], "x"], ",", "y", ",", 
   RowBox[{"{", "x", "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.785476692914056*^9, 3.7854767144723587`*^9}, 
   3.8537522666756687`*^9, {3.853752948184157*^9, 3.853752960040937*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"ce8c7618-20e2-4d14-9f60-106a480308a6"],

Cell[BoxData[
 RowBox[{
  RowBox[{"8", " ", "x"}], "+", 
  RowBox[{"7", " ", 
   SuperscriptBox["x", "2"]}], "-", 
  SuperscriptBox["x", "3"], "-", 
  RowBox[{"12", " ", 
   SuperscriptBox["x", "2"], " ", "y"}], "-", 
  RowBox[{"12", " ", 
   SuperscriptBox["x", "3"], " ", "y"}], "-", 
  RowBox[{"6", " ", 
   SuperscriptBox["x", "3"], " ", 
   SuperscriptBox["y", "2"]}], "+", 
  RowBox[{"3", " ", 
   SuperscriptBox["x", "4"], " ", 
   SuperscriptBox["y", "2"]}], "-", 
  RowBox[{"8", " ", 
   SuperscriptBox["x", "3"], " ", 
   SuperscriptBox["y", "3"]}], "+", 
  RowBox[{"10", " ", 
   SuperscriptBox["x", "4"], " ", 
   SuperscriptBox["y", "3"]}], "+", 
  RowBox[{"12", " ", 
   SuperscriptBox["x", "4"], " ", 
   SuperscriptBox["y", "4"]}], "-", 
  RowBox[{"3", " ", 
   SuperscriptBox["x", "5"], " ", 
   SuperscriptBox["y", "4"]}], "-", 
  RowBox[{"6", " ", 
   SuperscriptBox["x", "5"], " ", 
   SuperscriptBox["y", "5"]}], "+", 
  RowBox[{
   SuperscriptBox["x", "6"], " ", 
   SuperscriptBox["y", "6"]}]}]], "Output",
 CellChangeTimes->{
  3.785476665380383*^9, {3.785476712540224*^9, 3.785476715230508*^9}, 
   3.785476993794319*^9, {3.8537522525970783`*^9, 3.8537522691618013`*^9}, {
   3.853752950214363*^9, 3.8537529696938457`*^9}, 3.853753132728199*^9, 
   3.8537540213095303`*^9, 3.8587702839371033`*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"3a986118-e91e-4dd4-b82a-d37e5eabf2dd"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CheckPerfection", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"-", "2"}], " ", 
     SuperscriptBox["x", "4"]}], "+", 
    SuperscriptBox["y", "2"], "-", 
    RowBox[{"3", " ", 
     SuperscriptBox["x", "2"], " ", 
     SuperscriptBox["\[Eta]", "2"]}], "-", 
    SuperscriptBox["\[Eta]", "4"]}], ",", 
   RowBox[{"{", 
    RowBox[{"y", ",", "\[Eta]"}], "}"}]}], "]"}]], "Input",
 CellLabel->"In[5]:=",ExpressionUUID->"7e163e7a-ef45-4219-9aa8-70ed52421ab8"],

Cell[BoxData[
 RowBox[{"{", "False", "}"}]], "Output",
 CellChangeTimes->{3.785476871140481*^9, 3.785476993839501*^9, 
  3.853752282679962*^9, 3.853752969740346*^9, 3.853753132780683*^9, 
  3.853754021405301*^9, 3.858770284011257*^9},
 CellLabel->"Out[5]=",ExpressionUUID->"cf8875d4-6ec0-46e7-a5c0-e97ebd230d40"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"sirface", "[", "1", "]"}], "=", 
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{"z", "-", "u1", "-", "u2", "-", "u3"}], ")"}], "2"], "-", " ", 
    RowBox[{"4", "u1", " ", "u2", " ", "u3"}], " ", "-", 
    SuperscriptBox["u", "2"]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.853753881660502*^9, 3.853753926068664*^9}},
 CellLabel->"In[6]:=",ExpressionUUID->"53a13253-a348-432a-bbbc-3e5cc8eaddf3"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CheckPerfection", "[", 
  RowBox[{
   RowBox[{"sirface", "[", "1", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"u", ",", "u1", ",", "u2", ",", "u3"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.785476887937818*^9, 3.785476891979031*^9}, 
   3.85375390491873*^9},
 CellLabel->"In[7]:=",ExpressionUUID->"b8649134-a1c5-4abe-8fb5-7487e63836f8"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"True", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "z"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "z", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "z", ",", "0", ",", "0"}], "}"}]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.785476889531662*^9, 3.785476892355606*^9}, 
   3.785476993896538*^9, 3.853752287317775*^9, 3.853752969785325*^9, 
   3.853753132923411*^9, 3.853753905595347*^9, 3.853754021476186*^9, 
   3.8587702842185707`*^9},
 CellLabel->"Out[7]=",ExpressionUUID->"10fc8b56-9fac-4c3b-ac79-ace68134f376"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"sirface", "[", "2", "]"}], "=", 
   RowBox[{
    SuperscriptBox["x", "4"], "+", 
    RowBox[{"4", 
     SuperscriptBox["x", "2"], 
     SuperscriptBox["y", "2"]}], "+", 
    RowBox[{"4", 
     SuperscriptBox["z", "4"]}], "-", 
    RowBox[{"4", 
     SuperscriptBox["u", "2"], 
     SuperscriptBox["x", "2"]}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.853753912555622*^9, 3.853753923547861*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"7987004d-a2c3-4260-b3bb-42ab99892680"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CheckPerfection", "[", 
  RowBox[{
   RowBox[{"sirface", "[", "2", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"u", ",", "x", ",", "y", ",", "z"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.785477239013919*^9, 3.785477273661962*^9}, 
   3.853753921934745*^9},
 CellLabel->"In[9]:=",ExpressionUUID->"a3ccf881-bf48-42de-969a-8f2d5b6250a7"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"True", ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"u", ",", "0", ",", 
         RowBox[{"-", "u"}], ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"u", "\[NotEqual]", "0"}], "}"}], "}"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"u", ",", "0", ",", "u", ",", "0"}], "}"}], ",", 
       RowBox[{"{", 
        RowBox[{"{", 
         RowBox[{"u", "\[NotEqual]", "0"}], "}"}], "}"}]}], "}"}]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.78547727411469*^9, 3.853752291453308*^9, 
  3.853752969907268*^9, 3.8537531329706717`*^9, 3.853753929765379*^9, 
  3.8537540215522842`*^9, 3.8587702845728273`*^9},
 CellLabel->"Out[9]=",ExpressionUUID->"5122c0e7-fb84-42f2-a591-bc5c30a93d0e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"srpr", "[", "1", "]"}], "=", 
  RowBox[{"RationalizationNotInfinity", "[", 
   RowBox[{
    RowBox[{"sirface", "[", "1", "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"u", ",", "u1", ",", "u2", ",", "u3"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "z", ",", "0", ",", "0"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.785476941297584*^9, 3.78547694594597*^9}, {
  3.785477375045896*^9, 3.785477376180296*^9}, {3.785477708580319*^9, 
  3.785477709009695*^9}, {3.853753939814392*^9, 3.85375396674771*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"da8d1571-03dc-413b-81ee-d0a330ebc392"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"u", "\[Rule]", 
    FractionBox[
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         RowBox[{"t", "[", "1", "]"}], "+", 
         RowBox[{"t", "[", "2", "]"}]}], ")"}], "2"], "+", 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"t", "[", "1", "]"}], "+", 
         RowBox[{"t", "[", "2", "]"}], "-", 
         RowBox[{"2", " ", "z", " ", 
          RowBox[{"t", "[", "2", "]"}]}]}], ")"}], " ", 
       RowBox[{"t", "[", "3", "]"}]}], "+", 
      SuperscriptBox[
       RowBox[{"t", "[", "3", "]"}], "2"]}], 
     RowBox[{"4", " ", 
      RowBox[{"t", "[", "1", "]"}], " ", 
      RowBox[{"t", "[", "2", "]"}], " ", 
      RowBox[{"t", "[", "3", "]"}]}]]}], ",", 
   RowBox[{"u1", "\[Rule]", 
    FractionBox[
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"-", "1"}], "+", 
        RowBox[{"t", "[", "1", "]"}], "+", 
        RowBox[{"t", "[", "2", "]"}], "+", 
        RowBox[{"t", "[", "3", "]"}]}], ")"}], " ", 
      RowBox[{"(", 
       RowBox[{"1", "+", 
        RowBox[{"t", "[", "1", "]"}], "+", 
        RowBox[{"t", "[", "2", "]"}], "+", 
        RowBox[{"t", "[", "3", "]"}]}], ")"}]}], 
     RowBox[{"4", " ", 
      RowBox[{"t", "[", "2", "]"}], " ", 
      RowBox[{"t", "[", "3", "]"}]}]]}], ",", 
   RowBox[{"u2", "\[Rule]", 
    FractionBox[
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         RowBox[{"t", "[", "1", "]"}], "+", 
         RowBox[{"t", "[", "2", "]"}]}], ")"}], "2"], "+", 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"t", "[", "1", "]"}], "+", 
         RowBox[{"t", "[", "2", "]"}], "-", 
         RowBox[{"2", " ", "z", " ", 
          RowBox[{"t", "[", "2", "]"}]}]}], ")"}], " ", 
       RowBox[{"t", "[", "3", "]"}]}], "+", 
      SuperscriptBox[
       RowBox[{"t", "[", "3", "]"}], "2"]}], 
     RowBox[{"4", " ", 
      RowBox[{"t", "[", "1", "]"}], " ", 
      RowBox[{"t", "[", "3", "]"}]}]]}], ",", 
   RowBox[{"u3", "\[Rule]", 
    FractionBox[
     RowBox[{
      RowBox[{"-", "1"}], "+", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         RowBox[{"t", "[", "1", "]"}], "+", 
         RowBox[{"t", "[", "2", "]"}]}], ")"}], "2"], "+", 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"t", "[", "1", "]"}], "+", 
         RowBox[{"t", "[", "2", "]"}], "-", 
         RowBox[{"2", " ", "z", " ", 
          RowBox[{"t", "[", "2", "]"}]}]}], ")"}], " ", 
       RowBox[{"t", "[", "3", "]"}]}], "+", 
      SuperscriptBox[
       RowBox[{"t", "[", "3", "]"}], "2"]}], 
     RowBox[{"4", " ", 
      RowBox[{"t", "[", "1", "]"}], " ", 
      RowBox[{"t", "[", "2", "]"}]}]]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.785476946513445*^9, 3.7854769940314207`*^9, 3.785477376670224*^9, 
   3.78547782331739*^9, 3.853752335018054*^9, 3.8537529700607147`*^9, 
   3.853753133106369*^9, {3.8537539421442013`*^9, 3.853753967340927*^9}, 
   3.853754021690501*^9, 3.858770284818779*^9},
 CellLabel->"Out[10]=",ExpressionUUID->"385e6cea-86f8-43cf-a9f3-754c39bcd311"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"srpr", "[", "2", "]"}], "=", 
  RowBox[{"RationalizationNotInfinity", "[", 
   RowBox[{
    RowBox[{"sirface", "[", "2", "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"u", ",", "x", ",", "y", ",", "z"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "0", ",", 
      RowBox[{"-", "1"}], ",", "0"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.853752423002408*^9, 3.853752449505048*^9}, {
  3.853753945870613*^9, 3.853753947459572*^9}, {3.853753989533848*^9, 
  3.853753990491572*^9}},
 CellLabel->"In[11]:=",ExpressionUUID->"fc2713c6-eeb6-42ec-8719-a883e68e9197"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"u", "\[Rule]", 
    RowBox[{"1", "+", 
     FractionBox[
      RowBox[{"8", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "2"], " ", 
       RowBox[{"(", 
        RowBox[{"1", "+", 
         RowBox[{"t", "[", "2", "]"}]}], ")"}]}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "4"], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "1", "]"}], "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          SuperscriptBox[
           RowBox[{"t", "[", "2", "]"}], "2"]}], ")"}]}], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "3", "]"}], "4"]}]}]]}]}], ",", 
   RowBox[{"x", "\[Rule]", 
    FractionBox[
     RowBox[{"8", " ", 
      SuperscriptBox[
       RowBox[{"t", "[", "1", "]"}], "3"], " ", 
      RowBox[{"(", 
       RowBox[{"1", "+", 
        RowBox[{"t", "[", "2", "]"}]}], ")"}]}], 
     RowBox[{
      SuperscriptBox[
       RowBox[{"t", "[", "1", "]"}], "4"], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         SuperscriptBox[
          RowBox[{"t", "[", "2", "]"}], "2"]}], ")"}]}], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "3", "]"}], "4"]}]}]]}], ",", 
   RowBox[{"y", "\[Rule]", 
    RowBox[{
     RowBox[{"-", "1"}], "+", 
     FractionBox[
      RowBox[{"8", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "2"], " ", 
       RowBox[{"t", "[", "2", "]"}], " ", 
       RowBox[{"(", 
        RowBox[{"1", "+", 
         RowBox[{"t", "[", "2", "]"}]}], ")"}]}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "4"], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "1", "]"}], "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          SuperscriptBox[
           RowBox[{"t", "[", "2", "]"}], "2"]}], ")"}]}], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "3", "]"}], "4"]}]}]]}]}], ",", 
   RowBox[{"z", "\[Rule]", 
    FractionBox[
     RowBox[{"8", " ", 
      SuperscriptBox[
       RowBox[{"t", "[", "1", "]"}], "2"], " ", 
      RowBox[{"(", 
       RowBox[{"1", "+", 
        RowBox[{"t", "[", "2", "]"}]}], ")"}], " ", 
      RowBox[{"t", "[", "3", "]"}]}], 
     RowBox[{
      SuperscriptBox[
       RowBox[{"t", "[", "1", "]"}], "4"], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         SuperscriptBox[
          RowBox[{"t", "[", "2", "]"}], "2"]}], ")"}]}], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "3", "]"}], "4"]}]}]]}]}], "}"}]], "Output",
 CellChangeTimes->{{3.853752429986672*^9, 3.853752450664554*^9}, 
   3.853752970259116*^9, 3.853753133395306*^9, 3.853753948284813*^9, {
   3.853753992196615*^9, 3.8537540219566793`*^9}, 3.858770285094776*^9},
 CellLabel->"Out[11]=",ExpressionUUID->"239fc7f7-001f-414d-8f70-8d8ae856edaf"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"srpr", "[", "3", "]"}], "=", 
  RowBox[{"RationalizationNotInfinity", "[", 
   RowBox[{
    RowBox[{"sirface", "[", "2", "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"u", ",", "x", ",", "y", ",", "z"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"1", ",", "0", ",", "1", ",", "0"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{
  3.85375246096894*^9, {3.853753951782239*^9, 3.853753953643613*^9}, {
   3.853753994727861*^9, 3.853753995867661*^9}},
 CellLabel->"In[12]:=",ExpressionUUID->"9d73f07a-41fc-4ca8-a4cf-278ff7635277"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"u", "\[Rule]", 
    RowBox[{"1", "-", 
     FractionBox[
      RowBox[{"8", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         RowBox[{"t", "[", "2", "]"}]}], ")"}]}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "4"], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "1", "]"}], "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          SuperscriptBox[
           RowBox[{"t", "[", "2", "]"}], "2"]}], ")"}]}], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "3", "]"}], "4"]}]}]]}]}], ",", 
   RowBox[{"x", "\[Rule]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"8", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "3"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         RowBox[{"t", "[", "2", "]"}]}], ")"}]}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "4"], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "1", "]"}], "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          SuperscriptBox[
           RowBox[{"t", "[", "2", "]"}], "2"]}], ")"}]}], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "3", "]"}], "4"]}]}]]}]}], ",", 
   RowBox[{"y", "\[Rule]", 
    FractionBox[
     RowBox[{
      SuperscriptBox[
       RowBox[{"t", "[", "1", "]"}], "4"], "-", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "2"], " ", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          RowBox[{"t", "[", "2", "]"}]}], ")"}], "2"]}], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "3", "]"}], "4"]}]}], 
     RowBox[{
      SuperscriptBox[
       RowBox[{"t", "[", "1", "]"}], "4"], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         SuperscriptBox[
          RowBox[{"t", "[", "2", "]"}], "2"]}], ")"}]}], "+", 
      RowBox[{"4", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "3", "]"}], "4"]}]}]]}], ",", 
   RowBox[{"z", "\[Rule]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"8", " ", 
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", 
         RowBox[{"t", "[", "2", "]"}]}], ")"}], " ", 
       RowBox[{"t", "[", "3", "]"}]}], 
      RowBox[{
       SuperscriptBox[
        RowBox[{"t", "[", "1", "]"}], "4"], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "1", "]"}], "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "1"}], "+", 
          SuperscriptBox[
           RowBox[{"t", "[", "2", "]"}], "2"]}], ")"}]}], "+", 
       RowBox[{"4", " ", 
        SuperscriptBox[
         RowBox[{"t", "[", "3", "]"}], "4"]}]}]]}]}]}], "}"}]], "Output",
 CellChangeTimes->{
  3.853752461671143*^9, 3.853752970531986*^9, 3.8537531335704107`*^9, 
   3.8537539543096867`*^9, {3.853753996790852*^9, 3.853754022127392*^9}, 
   3.858770285369665*^9},
 CellLabel->"Out[12]=",ExpressionUUID->"6e95f324-3d81-4957-b5ba-335ec57c3125"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{
   RowBox[{"sirface", "[", "1", "]"}], "/.", 
   RowBox[{"srpr", "[", "1", "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.853753975660564*^9, 3.853753983403721*^9}},
 CellLabel->"In[13]:=",ExpressionUUID->"fc8e6ed9-75bb-4be7-b595-805482943216"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.853753977067134*^9, 3.853753983902646*^9}, 
   3.8537540221646*^9, 3.858770285425728*^9},
 CellLabel->"Out[13]=",ExpressionUUID->"d95f0b86-ae1c-4bc7-b74f-7d02765c4529"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{
   RowBox[{"sirface", "[", "2", "]"}], "/.", 
   RowBox[{"srpr", "[", "2", "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.8537540032835197`*^9, 3.853754004291394*^9}},
 CellLabel->"In[14]:=",ExpressionUUID->"c5c4bd2d-fd14-47c3-983e-b2ca8501284b"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.853754004759824*^9, 3.8537540222018747`*^9}, 
   3.858770285479897*^9},
 CellLabel->"Out[14]=",ExpressionUUID->"599fd8fa-a4e7-4e92-81ab-f235f6a978a5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{
   RowBox[{"sirface", "[", "2", "]"}], "/.", 
   RowBox[{"srpr", "[", "3", "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.8537540071236763`*^9, 3.8537540083076363`*^9}},
 CellLabel->"In[15]:=",ExpressionUUID->"fbf35fef-5c5a-41ba-9768-06c59ec97502"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{{3.853754008728115*^9, 3.853754022240206*^9}, 
   3.858770285523869*^9},
 CellLabel->"Out[15]=",ExpressionUUID->"ff3ed492-a6be-418c-ad86-8939167a6b7f"]
}, Open  ]]
},
WindowSize->{1849, 1025},
WindowMargins->{{0, Automatic}, {0, Automatic}},
Magnification:>1.5 Inherited,
FrontEndVersion->"12.0 for Linux x86 (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 575, 14, 82, "Input",ExpressionUUID->"e5fa4d96-81c3-4894-9128-e78798e58b24"],
Cell[CellGroupData[{
Cell[1180, 40, 400, 7, 37, "Print",ExpressionUUID->"3fa3a2ba-0a4d-4378-b2e6-c10887ef436e"],
Cell[1583, 49, 384, 6, 37, "Print",ExpressionUUID->"dc527305-85e1-43a8-976b-cc67f46b5908"],
Cell[1970, 57, 384, 6, 37, "Print",ExpressionUUID->"c1c14dd4-4966-4392-a0a4-aa497b1d50c5"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[2403, 69, 689, 18, 51, "Input",ExpressionUUID->"243984b7-19ca-4876-a5a6-b3e416cbf2d5"],
Cell[3095, 89, 571, 14, 53, "Output",ExpressionUUID->"c0093ab0-9074-4125-9b1f-29984182280e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3703, 108, 565, 14, 88, "Input",ExpressionUUID->"ce8c7618-20e2-4d14-9f60-106a480308a6"],
Cell[4271, 124, 1400, 39, 53, "Output",ExpressionUUID->"3a986118-e91e-4dd4-b82a-d37e5eabf2dd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5708, 168, 491, 14, 51, "Input",ExpressionUUID->"7e163e7a-ef45-4219-9aa8-70ed52421ab8"],
Cell[6202, 184, 312, 5, 53, "Output",ExpressionUUID->"cf8875d4-6ec0-46e7-a5c0-e97ebd230d40"]
}, Open  ]],
Cell[6529, 192, 461, 11, 47, "Input",ExpressionUUID->"53a13253-a348-432a-bbbc-3e5cc8eaddf3"],
Cell[CellGroupData[{
Cell[7015, 207, 367, 8, 47, "Input",ExpressionUUID->"b8649134-a1c5-4abe-8fb5-7487e63836f8"],
Cell[7385, 217, 669, 16, 53, "Output",ExpressionUUID->"10fc8b56-9fac-4c3b-ac79-ace68134f376"]
}, Open  ]],
Cell[8069, 236, 521, 15, 47, "Input",ExpressionUUID->"7987004d-a2c3-4260-b3bb-42ab99892680"],
Cell[CellGroupData[{
Cell[8615, 255, 365, 8, 47, "Input",ExpressionUUID->"a3ccf881-bf48-42de-969a-8f2d5b6250a7"],
Cell[8983, 265, 870, 24, 53, "Output",ExpressionUUID->"5122c0e7-fb84-42f2-a591-bc5c30a93d0e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9890, 294, 635, 13, 47, "Input",ExpressionUUID->"da8d1571-03dc-413b-81ee-d0a330ebc392"],
Cell[10528, 309, 3218, 92, 152, "Output",ExpressionUUID->"385e6cea-86f8-43cf-a9f3-754c39bcd311"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13783, 406, 610, 14, 47, "Input",ExpressionUUID->"fc2713c6-eeb6-42ec-8719-a883e68e9197"],
Cell[14396, 422, 3280, 99, 168, "Output",ExpressionUUID->"239fc7f7-001f-414d-8f70-8d8ae856edaf"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17713, 526, 564, 13, 47, "Input",ExpressionUUID->"9d73f07a-41fc-4ca8-a4cf-278ff7635277"],
Cell[18280, 541, 3560, 110, 168, "Output",ExpressionUUID->"6e95f324-3d81-4957-b5ba-335ec57c3125"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21877, 656, 301, 6, 47, "Input",ExpressionUUID->"fc8e6ed9-75bb-4be7-b595-805482943216"],
Cell[22181, 664, 219, 3, 53, "Output",ExpressionUUID->"d95f0b86-ae1c-4bc7-b74f-7d02765c4529"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22437, 672, 303, 6, 47, "Input",ExpressionUUID->"c5c4bd2d-fd14-47c3-983e-b2ca8501284b"],
Cell[22743, 680, 201, 3, 53, "Output",ExpressionUUID->"599fd8fa-a4e7-4e92-81ab-f235f6a978a5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22981, 688, 305, 6, 47, "Input",ExpressionUUID->"fbf35fef-5c5a-41ba-9768-06c59ec97502"],
Cell[23289, 696, 199, 3, 53, "Output",ExpressionUUID->"ff3ed492-a6be-418c-ad86-8939167a6b7f"]
}, Open  ]]
}
]
*)

