Print["Matrix for the differential systems wrt x in epsilon form for the case of banana."]
{{(14*x*y1^3*\[Epsilon] + 10*x^3*y1^3*\[Epsilon] - 10*y1^2*y2*\[Epsilon] + 
    89*x*y1^2*y2*\[Epsilon] - 32*x^2*y1^2*y2*\[Epsilon] - 
    5*x^3*y1^2*y2*\[Epsilon] - 6*x^4*y1^2*y2*\[Epsilon] - 
    12*y1^4*y2*\[Epsilon] - 24*x*y1^4*y2*\[Epsilon] + 
    24*x^3*y1^4*y2*\[Epsilon] + 12*x^4*y1^4*y2*\[Epsilon] - 
    55*y1*y2^2*\[Epsilon] + 14*x*y1*y2^2*\[Epsilon] - 
    112*x^2*y1*y2^2*\[Epsilon] + 10*x^3*y1*y2^2*\[Epsilon] - 
    x^4*y1*y2^2*\[Epsilon] + 2*y1^3*y2^2*\[Epsilon] + 
    4*x*y1^3*y2^2*\[Epsilon] - 4*x^3*y1^3*y2^2*\[Epsilon] - 
    2*x^4*y1^3*y2^2*\[Epsilon] + 49*x*y2^3*\[Epsilon] + 
    35*x^3*y2^3*\[Epsilon] + 42*y1^2*y2^3*\[Epsilon] + 
    84*x*y1^2*y2^3*\[Epsilon] - 84*x^3*y1^2*y2^3*\[Epsilon] - 
    42*x^4*y1^2*y2^3*\[Epsilon] - 42*y1*y2^4*\[Epsilon] - 
    84*x*y1*y2^4*\[Epsilon] + 84*x^3*y1*y2^4*\[Epsilon] + 
    42*x^4*y1*y2^4*\[Epsilon])/((-1 + x)*x*(1 + x)*(x*y1 - y2)*(2*y1 + 7*y2)*
    (-y1 + x*y2)), (4*(y1 - y2)*(y1 + y2)*(y2 + 7*x*y2 + x^2*y2 + 5*y1*y2^2 + 
     10*x*y1*y2^2 + 5*x^2*y1*y2^2)*\[Epsilon])/(x*(x*y1 - y2)*(2*y1 + 7*y2)*
    (-y1 + x*y2)), (2*(2*x*y1 - y2 - x^2*y2 + 2*y1^2*y2 + 4*x*y1^2*y2 + 
     2*x^2*y1^2*y2 + 2*y1*y2^2 + 4*x*y1*y2^2 + 2*x^2*y1*y2^2)*\[Epsilon])/
   (x*y1*(x*y1 - y2)*(-y1 + x*y2))}, 
 {-(((4*x*y1^4 + 2*x^3*y1^4 + 54*x*y1^6 + 30*x^3*y1^6 - 4*y1^3*y2 + 
      20*x*y1^3*y2 - 8*x^2*y1^3*y2 - 5*x^3*y1^3*y2 - 38*y1^5*y2 + 
      329*x*y1^5*y2 - 118*x^2*y1^5*y2 - 35*x^3*y1^5*y2 - 12*x^4*y1^5*y2 - 
      48*y1^7*y2 - 84*x*y1^7*y2 + 12*x^2*y1^7*y2 + 84*x^3*y1^7*y2 + 
      36*x^4*y1^7*y2 - 20*y1^2*y2^2 - 18*x*y1^2*y2^2 - 10*x^2*y1^2*y2^2 + 
      3*x^3*y1^2*y2^2 - 193*y1^4*y2^2 + 128*x*y1^4*y2^2 - 417*x^2*y1^4*y2^2 + 
      40*x^3*y1^4*y2^2 + 22*x^4*y1^4*y2^2 - 4*y1^6*y2^2 + 14*x*y1^6*y2^2 + 
      22*x^2*y1^6*y2^2 - 14*x^3*y1^6*y2^2 - 18*x^4*y1^6*y2^2 + 24*y1*y2^3 + 
      15*x*y1*y2^3 + 18*x^2*y1*y2^3 - 60*y1^3*y2^3 + 516*x*y1^3*y2^3 - 
      90*x^2*y1^3*y2^3 + 72*x^3*y1^3*y2^3 - 18*x^4*y1^3*y2^3 + 
      122*y1^5*y2^3 + 210*x*y1^5*y2^3 - 34*x^2*y1^5*y2^3 - 
      210*x^3*y1^5*y2^3 - 88*x^4*y1^5*y2^3 - 21*x*y2^4 - 234*y1^2*y2^4 + 
      33*x*y1^2*y2^4 - 372*x^2*y1^2*y2^4 + 51*x^3*y1^2*y2^4 + 
      18*x^4*y1^2*y2^4 - 118*y1^4*y2^4 - 280*x*y1^4*y2^4 - 44*x^2*y1^4*y2^4 + 
      280*x^3*y1^4*y2^4 + 162*x^4*y1^4*y2^4 + 21*y1*y2^5 + 210*x*y1*y2^5 - 
      21*x^2*y1*y2^5 + 84*x^3*y1*y2^5 + 126*y1^3*y2^5 + 294*x*y1^3*y2^5 + 
      42*x^2*y1^3*y2^5 - 294*x^3*y1^3*y2^5 - 168*x^4*y1^3*y2^5 - 
      168*y1^2*y2^6 - 294*x*y1^2*y2^6 + 42*x^2*y1^2*y2^6 + 
      294*x^3*y1^2*y2^6 + 126*x^4*y1^2*y2^6)*\[Epsilon])/
    ((-1 + x)*x*(1 + x)*(y1 - y2)*(x*y1 - y2)*(2*y1 + 7*y2)*(-y1 + x*y2))), 
  (-7*x*y1^2*y2*\[Epsilon] - 2*x^2*y1^2*y2*\[Epsilon] - 
    16*y1^4*y2*\[Epsilon] - 98*x*y1^4*y2*\[Epsilon] - 
    12*x^2*y1^4*y2*\[Epsilon] + 7*y1*y2^2*\[Epsilon] - 
    7*x^2*y1*y2^2*\[Epsilon] - 30*y1^3*y2^2*\[Epsilon] - 
    108*x*y1^3*y2^2*\[Epsilon] - 8*x^2*y1^3*y2^2*\[Epsilon] - 
    80*y1^5*y2^2*\[Epsilon] - 140*x*y1^5*y2^2*\[Epsilon] - 
    60*x^2*y1^5*y2^2*\[Epsilon] + 2*y2^3*\[Epsilon] + 7*x*y2^3*\[Epsilon] - 
    20*y1^2*y2^3*\[Epsilon] - 98*x*y1^2*y2^3*\[Epsilon] - 
    8*x^2*y1^2*y2^3*\[Epsilon] - 100*y1^4*y2^3*\[Epsilon] - 
    140*x*y1^4*y2^3*\[Epsilon] - 40*x^2*y1^4*y2^3*\[Epsilon] - 
    6*y1*y2^4*\[Epsilon] - 88*x*y1*y2^4*\[Epsilon] - 
    12*x^2*y1*y2^4*\[Epsilon] - 100*y1^3*y2^4*\[Epsilon] - 
    140*x*y1^3*y2^4*\[Epsilon] - 40*x^2*y1^3*y2^4*\[Epsilon] - 
    80*y1^2*y2^5*\[Epsilon] - 140*x*y1^2*y2^5*\[Epsilon] - 
    60*x^2*y1^2*y2^5*\[Epsilon])/(x*(x*y1 - y2)*(2*y1 + 7*y2)*(-y1 + x*y2)), 
  (-(x*y1^2*\[Epsilon]) - 14*x*y1^4*\[Epsilon] + y1*y2*\[Epsilon] + 
    x*y1*y2*\[Epsilon] + 6*y1^3*y2*\[Epsilon] - 2*x*y1^3*y2*\[Epsilon] + 
    6*x^2*y1^3*y2*\[Epsilon] - 16*y1^5*y2*\[Epsilon] - 
    28*x*y1^5*y2*\[Epsilon] - 12*x^2*y1^5*y2*\[Epsilon] - y2^2*\[Epsilon] + 
    2*y1^2*y2^2*\[Epsilon] - 14*x*y1^2*y2^2*\[Epsilon] - 
    2*x^2*y1^2*y2^2*\[Epsilon] - 20*y1^4*y2^2*\[Epsilon] - 
    28*x*y1^4*y2^2*\[Epsilon] - 8*x^2*y1^4*y2^2*\[Epsilon] + 
    10*y1*y2^3*\[Epsilon] + 2*x*y1*y2^3*\[Epsilon] + 
    6*x^2*y1*y2^3*\[Epsilon] - 20*y1^3*y2^3*\[Epsilon] - 
    28*x*y1^3*y2^3*\[Epsilon] - 8*x^2*y1^3*y2^3*\[Epsilon] - 
    16*y1^2*y2^4*\[Epsilon] - 28*x*y1^2*y2^4*\[Epsilon] - 
    12*x^2*y1^2*y2^4*\[Epsilon])/(x*y1*(y1 - y2)*(x*y1 - y2)*(-y1 + x*y2))}, 
 {(12*x*y1^6*\[Epsilon] - 8*y1^5*y2*\[Epsilon] + 272*x*y1^5*y2*\[Epsilon] - 
    16*x^2*y1^5*y2*\[Epsilon] + 94*x^3*y1^5*y2*\[Epsilon] - 
    12*y1^7*y2*\[Epsilon] + 258*x*y1^7*y2*\[Epsilon] + 
    12*x^2*y1^7*y2*\[Epsilon] + 162*x^3*y1^7*y2*\[Epsilon] - 
    184*y1^4*y2^2*\[Epsilon] + 1291*x*y1^4*y2^2*\[Epsilon] - 
    508*x^2*y1^4*y2^2*\[Epsilon] - 133*x^3*y1^4*y2^2*\[Epsilon] - 
    40*x^4*y1^4*y2^2*\[Epsilon] - 362*y1^6*y2^2*\[Epsilon] + 
    1353*x*y1^6*y2^2*\[Epsilon] - 538*x^2*y1^6*y2^2*\[Epsilon] + 
    117*x^3*y1^6*y2^2*\[Epsilon] + 60*x^4*y1^6*y2^2*\[Epsilon] - 
    240*y1^8*y2^2*\[Epsilon] - 420*x*y1^8*y2^2*\[Epsilon] + 
    60*x^2*y1^8*y2^2*\[Epsilon] + 420*x^3*y1^8*y2^2*\[Epsilon] + 
    180*x^4*y1^8*y2^2*\[Epsilon] - 824*y1^3*y2^3*\[Epsilon] + 
    286*x*y1^3*y2^3*\[Epsilon] - 1528*x^2*y1^3*y2^3*\[Epsilon] + 
    122*x^3*y1^3*y2^3*\[Epsilon] + 60*x^4*y1^3*y2^3*\[Epsilon] - 
    936*y1^5*y2^3*\[Epsilon] + 689*x*y1^5*y2^3*\[Epsilon] - 
    2094*x^2*y1^5*y2^3*\[Epsilon] + 151*x^3*y1^5*y2^3*\[Epsilon] + 
    90*x^4*y1^5*y2^3*\[Epsilon] - 20*y1^7*y2^3*\[Epsilon] + 
    70*x*y1^7*y2^3*\[Epsilon] + 110*x^2*y1^7*y2^3*\[Epsilon] - 
    70*x^3*y1^7*y2^3*\[Epsilon] - 90*x^4*y1^7*y2^3*\[Epsilon] - 
    48*y1^2*y2^4*\[Epsilon] + 747*x*y1^2*y2^4*\[Epsilon] - 
    36*x^2*y1^2*y2^4*\[Epsilon] + 399*x^3*y1^2*y2^4*\[Epsilon] + 
    274*y1^4*y2^4*\[Epsilon] + 3574*x*y1^4*y2^4*\[Epsilon] - 
    604*x^2*y1^4*y2^4*\[Epsilon] - 634*x^3*y1^4*y2^4*\[Epsilon] - 
    510*x^4*y1^4*y2^4*\[Epsilon] + 610*y1^6*y2^4*\[Epsilon] + 
    1050*x*y1^6*y2^4*\[Epsilon] - 170*x^2*y1^6*y2^4*\[Epsilon] - 
    1050*x^3*y1^6*y2^4*\[Epsilon] - 440*x^4*y1^6*y2^4*\[Epsilon] + 
    42*x*y1*y2^5*\[Epsilon] - 1632*y1^3*y2^5*\[Epsilon] - 
    717*x*y1^3*y2^5*\[Epsilon] - 1818*x^2*y1^3*y2^5*\[Epsilon] + 
    1137*x^3*y1^3*y2^5*\[Epsilon] + 510*x^4*y1^3*y2^5*\[Epsilon] - 
    590*y1^5*y2^5*\[Epsilon] - 1400*x*y1^5*y2^5*\[Epsilon] - 
    220*x^2*y1^5*y2^5*\[Epsilon] + 1400*x^3*y1^5*y2^5*\[Epsilon] + 
    810*x^4*y1^5*y2^5*\[Epsilon] - 42*y1^2*y2^6*\[Epsilon] + 
    903*x*y1^2*y2^6*\[Epsilon] + 42*x^2*y1^2*y2^6*\[Epsilon] + 
    567*x^3*y1^2*y2^6*\[Epsilon] + 630*y1^4*y2^6*\[Epsilon] + 
    1470*x*y1^4*y2^6*\[Epsilon] + 210*x^2*y1^4*y2^6*\[Epsilon] - 
    1470*x^3*y1^4*y2^6*\[Epsilon] - 840*x^4*y1^4*y2^6*\[Epsilon] - 
    840*y1^3*y2^7*\[Epsilon] - 1470*x*y1^3*y2^7*\[Epsilon] + 
    210*x^2*y1^3*y2^7*\[Epsilon] + 1470*x^3*y1^3*y2^7*\[Epsilon] + 
    630*x^4*y1^3*y2^7*\[Epsilon])/((-1 + x)*x*(1 + x)*(x*y1 - y2)*
    (2*y1 + 7*y2)^2*(-y1 + x*y2)), 
  (2*(2*y1^5*y2*\[Epsilon] + 7*x*y1^5*y2*\[Epsilon] + 
     34*y1^4*y2^2*\[Epsilon] + 189*x*y1^4*y2^2*\[Epsilon] + 
     20*x^2*y1^4*y2^2*\[Epsilon] + 50*y1^6*y2^2*\[Epsilon] + 
     255*x*y1^6*y2^2*\[Epsilon] + 30*x^2*y1^6*y2^2*\[Epsilon] + 
     180*y1^5*y2^3*\[Epsilon] + 270*x*y1^5*y2^3*\[Epsilon] + 
     90*x^2*y1^5*y2^3*\[Epsilon] + 200*y1^7*y2^3*\[Epsilon] + 
     350*x*y1^7*y2^3*\[Epsilon] + 150*x^2*y1^7*y2^3*\[Epsilon] - 
     34*y1^2*y2^4*\[Epsilon] - 189*x*y1^2*y2^4*\[Epsilon] - 
     20*x^2*y1^2*y2^4*\[Epsilon] + 50*y1^6*y2^4*\[Epsilon] - 
     50*x^2*y1^6*y2^4*\[Epsilon] - 2*y1*y2^5*\[Epsilon] - 
     7*x*y1*y2^5*\[Epsilon] - 180*y1^3*y2^5*\[Epsilon] - 
     270*x*y1^3*y2^5*\[Epsilon] - 90*x^2*y1^3*y2^5*\[Epsilon] - 
     50*y1^2*y2^6*\[Epsilon] - 255*x*y1^2*y2^6*\[Epsilon] - 
     30*x^2*y1^2*y2^6*\[Epsilon] - 50*y1^4*y2^6*\[Epsilon] + 
     50*x^2*y1^4*y2^6*\[Epsilon] - 200*y1^3*y2^7*\[Epsilon] - 
     350*x*y1^3*y2^7*\[Epsilon] - 150*x^2*y1^3*y2^7*\[Epsilon]))/
   (x*(x*y1 - y2)*(2*y1 + 7*y2)^2*(-y1 + x*y2)), 
  (2*x*y1^3*\[Epsilon] + 54*x*y1^2*y2*\[Epsilon] - 2*x^2*y1^2*y2*\[Epsilon] + 
    4*y1^4*y2*\[Epsilon] + 74*x*y1^4*y2*\[Epsilon] - 27*y1*y2^2*\[Epsilon] + 
    2*x*y1*y2^2*\[Epsilon] - 27*x^2*y1*y2^2*\[Epsilon] + 
    32*y1^3*y2^2*\[Epsilon] + 112*x*y1^3*y2^2*\[Epsilon] + 
    10*x^2*y1^3*y2^2*\[Epsilon] + 80*y1^5*y2^2*\[Epsilon] + 
    140*x*y1^5*y2^2*\[Epsilon] + 60*x^2*y1^5*y2^2*\[Epsilon] - 
    2*y2^3*\[Epsilon] + 62*y1^2*y2^3*\[Epsilon] + 
    182*x*y1^2*y2^3*\[Epsilon] + 50*x^2*y1^2*y2^3*\[Epsilon] + 
    100*y1^4*y2^3*\[Epsilon] + 140*x*y1^4*y2^3*\[Epsilon] + 
    40*x^2*y1^4*y2^3*\[Epsilon] - 36*y1*y2^4*\[Epsilon] + 
    4*x*y1*y2^4*\[Epsilon] - 30*x^2*y1*y2^4*\[Epsilon] + 
    100*y1^3*y2^4*\[Epsilon] + 140*x*y1^3*y2^4*\[Epsilon] + 
    40*x^2*y1^3*y2^4*\[Epsilon] + 80*y1^2*y2^5*\[Epsilon] + 
    140*x*y1^2*y2^5*\[Epsilon] + 60*x^2*y1^2*y2^5*\[Epsilon])/
   (x*(x*y1 - y2)*(2*y1 + 7*y2)*(-y1 + x*y2))}}
