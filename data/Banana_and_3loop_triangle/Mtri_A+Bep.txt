Print["Matrix for the differential systems wrt s in A+B\[Epsilon] form for the case of triangle with two massive loops."]
{{0, 0, 0, 0, 0, 0, 0}, {-(\[Epsilon]/(-1 + s)), 
  -(((1 + s)*\[Epsilon])/((-1 + s)*s)), 0, 0, 0, 0, 0}, 
 {0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, (-1 - 3*\[Epsilon])/s, 
  (4*(1 + 4*\[Epsilon]))/s, 0, 0}, {0, 0, 0, (1 + 3*\[Epsilon])/((-4 + s)*s), 
  (-4 - s - 16*\[Epsilon] - 2*s*\[Epsilon])/((-4 + s)*s), 
  (3*(1 + 3*\[Epsilon]))/(-4 + s), 0}, {(-2*\[Epsilon])/((-16 + s)*s), 0, 0, 
  (-6*(1 + 2*\[Epsilon]))/((-16 + s)*(-4 + s)*s), 
  (2*(20 + s)*(1 + 2*\[Epsilon]))/((-16 + s)*(-4 + s)*s), 
  -((64 - 2*s + s^2 + 64*\[Epsilon] + 16*s*\[Epsilon] + s^2*\[Epsilon])/
    ((-16 + s)*(-4 + s)*s)), 0}, 
 {0, 0, 0, (-5 + s - 20*\[Epsilon] - 2*s*\[Epsilon])/(12*(-1 + s)*s), 
  (5*(1 + 4*\[Epsilon]))/(3*(-1 + s)*s), 0, (-\[Epsilon] - s*\[Epsilon])/
   ((-1 + s)*s)}}
