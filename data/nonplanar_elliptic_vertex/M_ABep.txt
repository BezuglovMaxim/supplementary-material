Print["Matrix M in A+Bep form for the differential equation, for the exact Frobenius method"]
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, -(\[CurlyEpsilon]/s), 0, 0, 0, 0, 0, 
  0, 0, 0, 0}, {0, 0, -((-1 + 2*\[CurlyEpsilon])/s), 
  (2*(-2 + 3*\[CurlyEpsilon]))/s, 0, 0, 0, 0, 0, 0, 0}, 
 {-\[CurlyEpsilon]/(4*(-1 + s)*s), 0, (-1 + 2*\[CurlyEpsilon])/
   (4*(-1 + s)*s), -(-3 + 4*\[CurlyEpsilon] + 2*s*\[CurlyEpsilon])/
   (2*(-1 + s)*s), 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, (-2*\[CurlyEpsilon])/s, 
  (2*(-1 + 2*\[CurlyEpsilon]))/s, 0, 0, 0, 0, 0}, 
 {-\[CurlyEpsilon]/(8*s*(1 + s)), -\[CurlyEpsilon]/(4*s*(1 + s)), 0, 0, 
  -(-1 + 3*\[CurlyEpsilon])/(4*s*(1 + s)), -(3 + 2*s - 4*\[CurlyEpsilon])/
   (2*s*(1 + s)), 0, 0, 0, 0, 0}, {0, 0, 0, \[CurlyEpsilon], 0, 0, 0, 0, 0, 
  0, 0}, {0, 0, -\[CurlyEpsilon]/(8*s), -((-1 + 2*s)*\[CurlyEpsilon])/(4*s), 
  0, \[CurlyEpsilon], -(-1 + \[CurlyEpsilon])/(2*s), -(\[CurlyEpsilon]/s), 0, 
  0, 0}, {0, 0, \[CurlyEpsilon]/(4*s), ((-1 + 2*s)*\[CurlyEpsilon])/(2*s), 0, 
  0, (-1 + \[CurlyEpsilon])/s, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 
  (-2*\[CurlyEpsilon])/s, (4*(1 + 2*\[CurlyEpsilon]))/(s*(4 + s))}, 
 {((-5 + 11*s + 4*s^2)*\[CurlyEpsilon])/(64*(-1 + s)*(1 + s)), 
  (-3*s*\[CurlyEpsilon])/(16*(1 + s)), 
  -(-5 - 6*\[CurlyEpsilon] + 16*s*\[CurlyEpsilon])/(64*(-1 + s)), 
  -(5 + 10*s + 6*\[CurlyEpsilon] - 68*s*\[CurlyEpsilon] + 
     32*s^2*\[CurlyEpsilon])/(32*(-1 + s)), (-3*s*(-1 + 3*\[CurlyEpsilon]))/
   (16*(1 + s)), (s*(-3 + 20*\[CurlyEpsilon] + 8*s*\[CurlyEpsilon]))/
   (8*(1 + s)), 1 - \[CurlyEpsilon], -\[CurlyEpsilon]/2, 0, 
  -(1 + 4*\[CurlyEpsilon])/(4*s), (4 + s + 4*\[CurlyEpsilon])/(s*(4 + s))}}
