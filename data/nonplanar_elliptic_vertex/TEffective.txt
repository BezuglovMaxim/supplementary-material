Print["Transformation matrix T for effective master integrals, for the DE method"]
{{\[Epsilon]^(-2), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
 {0, (-1 + \[Epsilon])/(\[Epsilon]^2*(-1 + 2*\[Epsilon])), 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0}, 
 {-((-1 + \[Epsilon])^2*(1 - 4*\[Epsilon] + 2*s*(-1 + t)*
       (-2 + 7*\[Epsilon])))/(4*s*(-1 + t)*\[Epsilon]^2*
    (-2 + 13*\[Epsilon] - 27*\[Epsilon]^2 + 18*\[Epsilon]^3)), 0, 
  ((-1 + 2*s*(-1 + t))*z1*(-1 + \[Epsilon])^2)/(4*(-1 + t)*\[Epsilon]*
    (-2 + 13*\[Epsilon] - 27*\[Epsilon]^2 + 18*\[Epsilon]^3)), 
  ((-1 + \[Epsilon])^2*(-1 + 4*\[Epsilon] + 2*s^2*(-1 + t)*
      (4 + 4*t*(-1 + z1) - 7*z1)*\[Epsilon] + 
     s*(4 + 7*(-2 + z1)*\[Epsilon] - 2*t*(2 + (-7 + 2*z1)*\[Epsilon]))))/
   (4*s*(-1 + t)*\[Epsilon]^2*(-2 + 13*\[Epsilon] - 27*\[Epsilon]^2 + 
     18*\[Epsilon]^3)), 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
 {-((-1 + \[Epsilon])^2*(1 - 4*\[Epsilon] + 2*s*(-1 + t)*
       (-1 + 3*\[Epsilon])))/(8*s*(-1 + t)*\[Epsilon]^2*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 0, -(z1*(-1 + \[Epsilon])^2)/
   (8*(-1 + t)*\[Epsilon]*(1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 
  -((-1 + \[Epsilon])^2*(1 + 2*s*(-1 + t) - 4*\[Epsilon] + 
      s*(10 - 7*z1 + 2*t*(-5 + 2*z1))*\[Epsilon]))/
   (8*s*(-1 + t)*\[Epsilon]^2*(1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 0, 
  0, 0, 0, 0, 0, 0}, 
 {-((-1 + \[Epsilon])^2*(-1 + 4*\[Epsilon] + 2*s*t*(-2 + 7*\[Epsilon])))/
   (4*s*t*\[Epsilon]^2*(-2 + 13*\[Epsilon] - 27*\[Epsilon]^2 + 
     18*\[Epsilon]^3)), 0, 0, 0, ((1 + 2*s*t)*z2*(-1 + \[Epsilon])^2)/
   (4*t*\[Epsilon]*(-2 + 13*\[Epsilon] - 27*\[Epsilon]^2 + 18*\[Epsilon]^3)), 
  -((-1 + \[Epsilon])^2*(-1 + 4*s*t + (4 - 2*s*t*(7 + 4*s*t) + 
        s*(3 + 4*t)*(1 + 2*s*t)*z2)*\[Epsilon]))/
   (4*s*t*\[Epsilon]^2*(-1 + 2*\[Epsilon])*(-2 + 3*\[Epsilon])*
    (-1 + 3*\[Epsilon])), 0, 0, 0, 0, 0, 0, 0}, 
 {-((-1 + \[Epsilon])^2*(-1 + 4*\[Epsilon] + 2*s*t*(-1 + 3*\[Epsilon])))/
   (8*s*t*\[Epsilon]^2*(1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 0, 
  (z2*(-1 + \[Epsilon])^2)/(8*t*\[Epsilon]*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 
  -((-1 + \[Epsilon])^2*(-1 + 2*s*t + (4 + 3*s*z2 + 2*s*t*(-5 + 2*z2))*
       \[Epsilon]))/(8*s*t*\[Epsilon]^2*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 0, 0, 0, 0, 0}, 
 {-(-1 + \[Epsilon])^2/(4*\[Epsilon]^2*(1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 
  -((-1 + \[Epsilon])^2/(\[Epsilon]^2*(1 + \[Epsilon]*(-5 + 6*\[Epsilon])))), 
  0, (-1 + \[Epsilon])^2/(4*\[Epsilon]^2*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 
  (z3*(-1 + \[Epsilon])^2)/(\[Epsilon]^2*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 
  ((-1 + \[Epsilon])^2*(-1 + (2 + 2*s*t*(-1 + z3) - s*z3)*\[Epsilon]))/
   (\[Epsilon]^3*(s + s*\[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 0, 0, 0}, 
 {0, 0, 0, 0, 0, 0, 0, (-1 + \[Epsilon])^2/(2*s*\[Epsilon]^3), 0, 0, 0, 0, 
  0}, {-(-1 + \[Epsilon])^2/(4*\[Epsilon]^2*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 
  -((-1 + \[Epsilon])^2/(\[Epsilon]^2*(1 + \[Epsilon]*(-5 + 6*\[Epsilon])))), 
  0, 0, 0, (-1 + \[Epsilon])^2/(4*\[Epsilon]^2*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 
  (z4*(-1 + \[Epsilon])^2)/(\[Epsilon]^2*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 
  -(((-1 + \[Epsilon])^2*(1 + (-2 + s*(2 + 2*t*(-1 + z4) - z4))*\[Epsilon]))/
    (\[Epsilon]^3*(s + s*\[Epsilon]*(-5 + 6*\[Epsilon])))), 0, 0, 0}, 
 {0, 0, 0, 0, 0, 0, 0, 0, 0, (-1 + \[Epsilon])^2/(2*s*\[Epsilon]^3), 0, 0, 
  0}, {((-1 + \[Epsilon])*(1 + \[Epsilon]*(-5 + 10*\[Epsilon] + 
       s*(-2 + 6*\[Epsilon]))))/(8*\[Epsilon]^3*
    (s + s*\[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 0, 
  (z2 - z2*\[Epsilon])/(4*t*\[Epsilon] - 20*t*\[Epsilon]^2 + 
    24*t*\[Epsilon]^3), 
  ((-1 + \[Epsilon])*(-1 + \[Epsilon]*(5 - 4*\[Epsilon] + 
       2*s*(t + 3*z2*\[Epsilon] + t*(-7 + 4*z2)*\[Epsilon]))))/
   (8*s*t*\[Epsilon]^3*(1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 0, 0, 
  (-1 + \[Epsilon])/(2*s*\[Epsilon]^3), 0, 0}, 
 {((-1 + \[Epsilon])*(1 + \[Epsilon]*(-5 + 10*\[Epsilon] + 
       s*(-2 + 6*\[Epsilon]))))/(8*\[Epsilon]^3*
    (s + s*\[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 
  (z1*(-1 + \[Epsilon]))/(4*(-1 + t)*\[Epsilon]*
    (1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 
  ((-1 + \[Epsilon])*(1 + \[Epsilon]*(-5 + 4*\[Epsilon] + 
       2*s*(-1 + t + (7 - 7*z1 + t*(-7 + 4*z1))*\[Epsilon]))))/
   (8*s*(-1 + t)*\[Epsilon]^3*(1 + \[Epsilon]*(-5 + 6*\[Epsilon]))), 0, 0, 0, 
  0, 0, 0, 0, (-1 + \[Epsilon])/(2*s*\[Epsilon]^3), 0}, 
 {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ((-1 + t)*t*z5*(-1 + \[Epsilon])^2)/
   (s*(-1 + s*(-1 + t)*t)*\[Epsilon]^3)}}
