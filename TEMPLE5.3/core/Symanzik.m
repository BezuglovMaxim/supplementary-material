(* ::Package:: *)

Begin["Private`"]


Symanzik[InternalMomenta_List,Propagators_List,RepRules_List:{},OptionsPattern[{FeynmanParameter->x}]]:=
Module[{polinom,U,F,M,Q,J,zerosubs,l,q1,q2,i},l=Length[InternalMomenta];polinom=Sum[OptionValue[FeynmanParameter][i]*Propagators[[i]],{i,1,Length[Propagators]}];
M=-Table[Table[D[D[polinom,InternalMomenta[[q1]]],InternalMomenta[[q2]]]/2,{q2,1,l}],{q1,1,l}];
U=Det[M]/.RepRules;
zerosubs=Table[InternalMomenta[[q1]]->0,{q1,1,l}]; 
Q=Table[D[polinom,InternalMomenta[[q1]]]/2/.zerosubs,{q1,1,l}];
J=polinom/.zerosubs;
F=Expand[Simplify[(U*(J+Q.Inverse[M].Q))/.RepRules]]; 
{U,F}
]; 
(*Calculates the two Symanzik polinomials U and F,Output in te form {U,F}*)


End[]
