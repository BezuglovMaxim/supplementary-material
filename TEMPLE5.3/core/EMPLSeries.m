(* ::Package:: *)

Begin["Private`"]


ySer[x_,n_,a_]:=Normal[Series[Sqrt[Product[x-a[i],{i,1,4}]],{x,0,n}]];

Z4Ser[x_,n_,a_]:=Module[{s1,s2,\[CapitalPhi]tidle,\[CapitalPhi]},
s1=SymmetricPolynomial[1,Table[a[i],{i,1,4}]];
       s2=SymmetricPolynomial[2,Table[a[i],{i,1,4}]];
       \[CapitalPhi]tidle[x1_]:=1/(c4[a]*ySer[x1,n,a]) (x1^2-s1/2 x1+s2/6);  
       \[CapitalPhi][x1_]:=\[CapitalPhi]tidle[x1]+4c4[a]*\[Eta]1[a]/\[Omega]1[a] 1/ySer[x1,n,a];    

Integrate[Normal[Series[\[CapitalPhi][x],{x,0,n-1}]],{x,0,x}]+Z4[0,a]
];

ExpandSingleE4[E4[l_,v_,ap_],v_,n_,OptionsPattern[{EllipticCurve-> y}]]:=Module[{k=Length[l],Integrand,x,series,seriesIndexes,i,s,Psi,yi=OptionValue[EllipticCurve],Z4p2},
(*********************************************************)
(******************ONE*****************************)
        Psi[0,0,x_,a_]:=c4[a]/(ySer[x,n,a] \[Omega]1[a]);
        Psi[1,c_,x_,a_]:=1/(x-c);
        Psi[-1,c_,x_,a_]:=yi[c]/(ySer[x,n,a](x-c)) +Z4[c,a] c4[a]/ySer[x,n,a];
        Psi[1,Infinity,x_,a_]:=-Z4Ser[x,n,a] c4[a]/ySer[x,n,a];
        Psi[-1,Infinity,x_,a_]:=x/ySer[x,n,a]-1/ySer[x,n,a] (a[1]+2*c4[a]Gstar[a]);
        (******************TWO*****************************)
        Psi[-2,c_,x_,a_]:=-((\[Omega]1[a]yi[c]Z4Ser[x,n,a])/(2(x-c)ySer[x,n,a]))+(Z4[c,a]\[Omega]1[a])/(2(x-c))-(c4[a]\[Omega]1[a]Z4[c,a]Z4Ser[x,n,a])/(2*ySer[x,n,a]);   
        Psi[-2,Infinity,x_,a_]:=\[Omega]1[a]/(2c4[a])-(\[Omega]1[a]Z4Ser[x,n,a]x)/(2ySer[x,n,a])+(\[Omega]1[a]a[1]Z4Ser[x,n,a])/(2 ySer[x,n,a])+(\[Omega]1[a]c4[a]Gstar[a]Z4Ser[x,n,a])/ySer[x,n,a];  
        Psi[2,Infinity,x_,a_]:=1/4 \[Omega]1[a]/(c4[a]ySer[x,n,a]) (8c4[a]^2 Z4p2[x,a]+4c4[a]Gstar[a](a[1]-x)+4c4[a]^2 Gstar[a]^2+2(-a[1]+a[2]+a[3]+a[4])x+a[1]^2-
          a[2]a[3]-a[2]a[4]-a[3]a[4]-2x^2)-1/2 \[Omega]1[a]Z4Ser[x,n,a]/(a[1]-x);
        (*************************************************************************************************)
        (**************ABRIVIATIONS*****************************)
           Z4p2[x_,a_]:=1/8 (Z4Ser[x,n,a])^2+((a[2]-x)(a[3]-x)(a[4]-x)Z4Ser[x,n,a])/(c4[a]x)+(-3a[3]x-3a[4]x-a[1](a[2]+a[3]+a[4]-3x)+a[2](2a[3]+2a[4]-3x)+2a[3]a[4]+3x^2)/(24c4[a]^2);
        (*******************************************************)
(*********************************************************)
Integrand=Product[Psi[l[[i]][[1]],l[[i]][[2]],x[i],ap],{i,1,k}];
seriesIndexes=Table[{x[i],0,n},{i,0,k}];  (*****Redundancy!!!! Need to by fixed****)

series=Normal[Series[Integrand,##] &@@seriesIndexes];

For[i=k,i>=1,i=i-1,
series=E4Primitive[series,x[i],EllipticCurve->yi,BranchPointsLetter->ap]/.{x[i]->x[i-1]};
];
s=series/.{x[0]->v};
((s/.{v->0})+Sum[Simplify[Coefficient[s,v^i]]v^i,{i,1,n}])//.{(ap[1] ap[2] ap[3] ap[4])^m_.-> yi[0]^(2*m),ap[1]^m_. ap[2]^m_. ap[3]^m_. ap[4]^m_.->yi[0]^(2*m) }
];

ExpandEMPLs[a_,x_,n_,OptionsPattern[{EllipticCurve-> y}]]:=Module[{b,l,yi=OptionValue[EllipticCurve]},
b=(a/.{E4[l_,x,ap_]:>   ExpandSingleE4[E4[l,x,ap],x,n,EllipticCurve-> yi],Z4[x,ap_]-> Z4Ser[x,n,ap]} );  
Normal[Series[b,{x,0,n}]]
]; 


End[]
