(* ::Package:: *)

DE4::usage="derivative"


Begin["Private`"]


(**********************************NOT FINISHED!!! only works in simple cases when letters do not contain variable******************************************************)


DE4[expr_,xx_,OptionsPattern[{EllipticCurve->y,BranchPointsLetter->a}]]:=Block[{Psi,i,dE4,yi=OptionValue[EllipticCurve],ai=OptionValue[BranchPointsLetter]},
(******************General rules******************************************************************)
dE4[e_,x_]:=D[e,x]/;FreeQ[e,E4]&&FreeQ[e,yi]&&FreeQ[e,Z4];
dE4[a_+b_,x_]:=dE4[a,x]+dE4[b,x];
dE4[a_*b_,x_]:=a*dE4[b,x]/;FreeQ[a,x]&&FreeQ[a,yi];
dE4[a_*b_,x_]:=dE4[a,x]b+dE4[b,x]a;
dE4[a_/b_,x_]:=(dE4[a,x]b-dE4[b,x]a)/b^2 ;
(*dE4[f_[g_],x_]:=D[f[g],g]dE4[g,x];*)
(*******************EMPL**************************************************************************)
dE4[E4[l_,x_,ai],x_]:=Psi[l[[1]][[1]],l[[1]][[2]],x,ai]E4[Delete[l,1],x,ai]/;FreeQ[l,x];
dE4[yi,x_]:=yi/2 Sum[1/(x-ai[i]),{i,1,4}];
dE4[Z4[x_,ai],x_]:=1/(c4[ai]*yi) (x^2-SymmetricPolynomial[1,Table[ai[i],{i,1,4}]]/2 x+ SymmetricPolynomial[2,Table[ai[i],{i,1,4}]]/6)+4c4[ai]*\[Eta]1[ai]/\[Omega]1[ai] 1/yi;
(*************************************************************************************************)
(**************************Cernels****************************************************************)
(*************************************************************************************************)
(******************ONE*****************************)
Psi[0,0,x_,a_]:=c4[a]/(yi \[Omega]1[a]);
Psi[1,c_,x_,a_]:=1/(x-c);
Psi[-1,c_,x_,a_]:=yi[c]/(yi(x-c)) +Z4[c,a] c4[a]/yi;
Psi[1,Infinity,x_,a_]:=-Z4[x,a] c4[a]/yi;
Psi[-1,Infinity,x_,a_]:=x/yi-1/yi (a[1]+2*c4[a]Gstar[a]);
(******************TWO*****************************)
Psi[-2,c_,x_,a_]:=-((\[Omega]1[a]yi[c]Z4[x,a])/(2(x-c)yi))+(Z4[c,a]\[Omega]1[a])/(2(x-c))-(c4[a]\[Omega]1[a]Z4[c,a]Z4[x,a])/(2*yi);   
Psi[-2,Infinity,x_,a_]:=\[Omega]1[a]/(2c4[a])-(\[Omega]1[a]Z4[x,a]x)/(2 yi)+(\[Omega]1[a]a[1]Z4[x,a])/(2 yi)+(\[Omega]1[a]c4[a]Gstar[a]Z4[x,a])/yi;  
Psi[2,Infinity,x_,a_]:=1/4 \[Omega]1[a]/(c4[a]yi) (8c4[a]^2 Z4p2[x,a]+4c4[a]Gstar[a](a[1]-x)+4c4[a]^2 Gstar[a]^2+2(-a[1]+a[2]+a[3]+a[4])x+a[1]^2-
a[2]a[3]-a[2]a[4]-a[3]a[4]-2x^2)-1/2 \[Omega]1[a] Z4[x,a]/(a[1]-x);
(*************************************************************************************************)
(**************ABRIVIATIONS*****************************)
Z4p2[x_,a_]:=1/8 (Z4[x,a])^2+((a[2]-x)(a[3]-x)(a[4]-x)Z4[x,a])/(c4[a]x)+(-3a[3]x-3a[4]x-a[1](a[2]+a[3]+a[4]-3x)+a[2](2a[3]+2a[4]-3x)+2a[3]a[4]+3x^2)/(24c4[a]^2);
(*******************************************************)
dE4[expr,xx]
];


End[]
