(* ::Package:: *)

(*Some particular Values of E4s *)


Begin["Private`"]


E4[{},x_,a_]:=1;
E4[l_,0,a_]:=0/;!ContainsOnly[l,{{1,0},{-1,0}}];
E4[l_,1,a_]:=0/;ContainsOnly[l,{{1,0}}];


E4[l_,0,a_]:=G[{##},0]&@@Table[0,{i,1,Length[l]}]/;ContainsOnly[l,{{1,0},{-1,0}}]


End[]
