(* ::Package:: *)

Begin["Private`"]


GenerateGIFromList[l_]:=Module[{},If[Length[l]<=2,Return[1]]; IG[l[[1]],l[[2;;-2]],l[[-1]]]]

GenerateAllPolygons[n_]:=Module[{l},
l=Flatten[Table[Permutations[Table[Piecewise[{{True,j<=i}},False],{j,1,n}]],{i,0,n}],1];
Table[Insert[l[[i]],True,{{1},{-1}}],{i,1,2^n}]
];

PolygonsOnSemiCircle[l_,pl_]:=Module[{i,n=Length[l],leftList={},rightLists={},findzero=False,zerocordinate=0},
For[i=1,i<= n,i++, If[pl[[i]],leftList=Insert[leftList,l[[i]],-1]]];
For[i=2,i<=n,i++,
If[findzero==False && pl[[i]]==False,findzero=True;zerocordinate=i];
If[findzero==True && pl[[i]]==True,rightLists=Insert[rightLists,l[[zerocordinate-1;;i]],-1];findzero=False];
];
If[rightLists=={},Return[{leftList,{{}}}]];
{leftList,rightLists}
];
(*******************************)
IGCoproduct[a_]:=IGCoproduct[GsAsIGs[a]];
IGCoproduct[a_]:=a/;FreeQ[a,Pi]&&FreeQ[a,G]&&FreeQ[a,IG]; 
IGCoproduct[Pi^n_.]:=TP[{Pi^n,1}]/;IntegerQ[n];   
IGCoproduct[Pi^n_.]:=Pi^n/;!IntegerQ[n]&&FreeQ[a,G]&&FreeQ[a,IG];  
IGCoproduct[a_+b_]:=IGCoproduct[a]+IGCoproduct[b];   
IGCoproduct[a_*b_]:=TPSimplify[IGCoproduct[a]*IGCoproduct[b]];    
IGCoproduct[a_^n_]:=TPSimplify[IGCoproduct[a]]^n;  
IGCoproduct[IG[a0_,l1_,an_]]:=Module[{n=Length[l1],polygonslist,l,li,s=0},
polygonslist=GenerateAllPolygons[n]; 
l=Insert[Insert[l1,an,-1],a0,1]; 
For[i=1,i<=2^n,i++,
li=PolygonsOnSemiCircle[l,polygonslist[[i]]];

s=s+TP[{GenerateGIFromList[li[[1]]],Product[GenerateGIFromList[li[[2]][[j]]],{j,1,Length[li[[2]]]}]}];
];
(*RegularizeG[IGsAsGs[s]]/.{G[larg_List,arg_]/;ContainsOnly[larg,{arg}]\[RuleDelayed] 0}*)
s
];

IGCoproduct[e_,l_List]:=Module[{s=0,c,cp,n},
c=IGCoproduct[e];
cp=List @@ c;
n=Length[cp];
For[i=1,i<=n,i++,
If[CheckWeightPattern[cp[[i]],l],s=s+cp[[i]]];
];
s
]/;Length[l]==2; 

IGCoproduct[e_,l_List]:=Module[{ln,c,BackOperator},
ln=Insert[Delete[l,{{-1},{-2}}],l[[-1]]+l[[-2]],-1];
c=IGCoproduct[e,ln];
(*********)
BackOperator[a_+b_]:=BackOperator[a]+BackOperator[b];
BackOperator[a_*TP[ll_]]:= a BackOperator[TP[ll]]/;FreeQ[a,TP];
BackOperator[TP[{a___,b_}]]:=TP[{a,IGCoproduct[b,{l[[-2]],l[[-1]]}]}];
(*********)
BackOperator[c]
]/;Length[l]>2;

GCoproduct[e_,l_List]:=Module[{},RegularizeG[ExpandAll[IGsAsGs[IGCoproduct[e,l]]]]/.{G[larg_List,arg_]/;ContainsOnly[larg,{arg}]:> 0}];
GCoproduct[e_]:=Module[{},RegularizeG[ExpandAll[IGsAsGs[IGCoproduct[e]]]]/.{G[larg_List,arg_]/;ContainsOnly[larg,{arg}]:> 0}];
(**********************************************************************************************)
Weight[a_*b_]:=Weight[a]+Weight[b];
Weight[a_]:=0/;FreeQ[a,G]&& FreeQ[a,E4]&&FreeQ[a,Pi]&&FreeQ[a,Log] &&FreeQ[a,IG];
Weight[a_^n_.]:=n*Weight[a];
Weight[Log[a_]^n_.]:=n;
Weight[Pi^n_.]:=n;
Weight[G[l_,a_]]:=Length[l];
Weight[IG[b_,l_,a_]]:=Length[l];
Weight[E4[l_,x_,a_]]:=Sum[l[[i]][[1]],{i,1,Length[l]}];
Weight[l_List]:=Table[Weight[l[[i]]],{i,1,Length[l]}];
TPWeight[TP[l_List]]:=Weight[l];
CheckWeightPattern[n_.*TP[l_List],a_List]:=(a== Weight[l]);
(**********************************************************************************************)

TP[{d___,a_+b_,c___}]:=TP[{d,a,c}]+TP[{d,b,c}];
TP[{a___,TP[{b___}],c___}]:=TP[{a,b,c}];
TP[{a___,b_*c_,d___}]:=b*TP[{a,c,d}]/;FreeQ[b,G]&&FreeQ[b,IG]&&FreeQ[b,E4]&&FreeQ[b,Pi]&&FreeQ[b,Log]&&FreeQ[b,I] ; 
TP[l_]:=0/; ContainsAny[l,{0}];

SymbolTP[{d___,a_*b_,c___}]:=SymbolTP[{d,a,c}]+SymbolTP[{d,b,c}]; 
SymbolTP[{d___,-1*b_,c___}]:=SymbolTP[{d,b,c}];
SymbolTP[{d___,a_^n_,c___}]:=n*SymbolTP[{d,a,c}]; 
SymbolTP[{d___,1,c___}]:=0;
SymbolTP[{d___,b_,c___}]:=0/; b^2==1; 
SymbolTP[{d___,b_,c___}]:=0/; b^3==1;
SymbolTP[{d___,b_,c___}]:=0/; b^4==1;
SymbolTP[{d___,b_,c___}]:=0/; b^5==1;
SymbolTP[{d___,b_,c___}]:=0/; b^6==1;
SymbolTP[{d___,b_,c___}]:=0/; b^7==1;
SymbolTP[{d___,b_,c___}]:=0/; b^8==1;
SymbolTP[{d___,b_,c___}]:=0/; b^9==1;
SymbolTP[{a___,0,b___}]:=0;

TPSimplify[e_]:=TPSimplify[Expand[e]];
TPSimplify[a_+b_]:=TPSimplify[a]+TPSimplify[b]; 
TPSimplify[a_. TP[l1_List]TP[l2_List]]:=TPSimplify[a*TP[Table[l1[[i]]l2[[i]],{i,1,Length[l1]}]]]/;Length[l1]==Length[l2];

TPSimplify[a_. TP[l1_List]^n_]:=TPSimplify[a*TP[Table[l1[[i]]l1[[i]],{i,1,Length[l1]}]]]/;n>1;

TPSimplify[a_]:=a/;FreeQ[a,TP];   
TPSimplify[a_.*TP[l_]]:=a*TP[l]/;FreeQ[a,TP]; 
(**********************)
ToCircleTimes[a_+b_]:=ToCircleTimes[a]+ToCircleTimes[b];
ToCircleTimes[a_*b_]:=ToCircleTimes[a]*ToCircleTimes[b];
ToCircleTimes[a_]:=a/;FreeQ[a,TP];
ToCircleTimes[TP[l_]]:=CircleTimes @@l;
(****************************************************************************************************)
LogArg[Log[a_]]:=a;
LogArg[IG[a_,{b_},c_]]:=(c-b)/(a-b)/;ToString[a]!=ToString[b ]&&ToString[c]!=ToString[b];
LogArg[IG[a_,{b_},c_]]:=c-b/;ToString[a]== ToString[b ]&&ToString[c]!=ToString[b];
LogArg[IG[a_,{b_},c_]]:=1/(a-b)/;ToString[a]!=ToString[b ]&&ToString[c]== ToString[b];
LogArg[IG[a_,{b_},c_]]:=1/;ToString[a]== ToString[b ]&&ToString[c]== ToString[b];
LogArg[G[{b_},c_]]:=LogArg[IG[0,{b},c]];

LogArg[a_+b_]:=LogArg[a]*LogArg[b];

LogArg[f_[a___]]:=LogArg/@f[a];
LogArg[a_]:=0/; FreeQ[a, IG] &&  FreeQ[a, G] && FreeQ[a, Log];

SymbolGenerator[b_]:= SymbolGenerator[Expand[b]];
SymbolGenerator[b_+c_]:= SymbolGenerator[b]+SymbolGenerator[c];
SymbolGenerator[TP[b_]]:=SymbolTP[Factor[Together[LogArg[b]]]];
SymbolGenerator[-TP[b_]]:=-SymbolTP[Factor[Together[LogArg[b]]]];
SymbolGenerator[c_*TP[b_]]:=c*SymbolTP[Factor[Together[LogArg[b]]]];


SymbolMPL[0]:=0;
SymbolMPL[a_+b_]:=SymbolMPL[a]+SymbolMPL[b];
SymbolMPL[a_]:=Module[{w=Weight[a],maxCoproduct},
maxCoproduct=RegularizeG[IGsAsGs[If[w>1,IGCoproduct[a,Table[1,{i,1,w}]],IGCoproduct[a]]]]/.{G[larg_List,arg_]/;ContainsOnly[larg,{arg}]:> 0};

SymbolGenerator[maxCoproduct]
];
(***********************************************************************)
CheckFactor[a_]:=Module[{},If[FullSimplify[a]==1,Return[True]];If[IntegerQ[(2Pi I)/FullSimplify[Log[a]]],Return[True]];False]; (** Checks if (a^n)=1 for some n \[Element] \[CapitalZeta] **)

(***********************************************************************)
SymbolLetters[e_,OptionsPattern[{ShowReplacements-> False}]]:=Block[{$RecursionLimit=120000,RawLetters,rl,n,i,j,letrep={},letters},
RawLetters[a_+b_]:=Union[Join[RawLetters[a],RawLetters[b]]];
RawLetters[SymbolTP[{a___}]]:={a};
RawLetters[aq_]:=RawLetters[Expand[aq]];
RawLetters[b_*SymbolTP[{a___}]]:={a};
rl=RawLetters[e];
(******* Raw letters find all letters *******)

(******* Next code exclude all letters which different by a factor f such that f^n=1 for some n \[Element] \[CapitalZeta] *******)
n=Length[rl];
For[i=1,i<= n,i++,
For[j=i,j<= n,j++,
If[CheckFactor[rl[[i]]/rl[[j]]] && i!=j, letrep=Join[letrep,{rl[[i]]->rl[[j]]}]];
rl=(rl//.letrep);
];
];
letters=Union[ rl//.letrep];
If[OptionValue[ShowReplacements],{letters,letrep},letters]
]; 

SymbolSimplify[e_]:=Module[{rep=SymbolLetters[e,ShowReplacements->True][[2]]},Simplify[e//.rep]];
SymbolSimplify[0]:=0;

(********************************************************************************)
(********************************************************************************)
TPLogariphms[e_,OptionsPattern[{ShowReplacements-> False}]]:=Block[{$RecursionLimit=120000,RawLetters,rl,erl,n,i,j,letrep={},logrep={},letters,logs},
RawLogs[a_+b_]:=Union[Join[RawLogs[a],RawLogs[b]]];
RawLogs[TP[{a_,b___}]]:={b};
RawLogs[aq_]:=RawLogs[Expand[aq]];
RawLogs[c_*TP[{a_,b___}]]:={b};
rl=RawLogs[e];
erl=Exp[rl];
(******* Raw logs find all logs *******)

(******* Next code exclude all logs which arguments are different by a factor f such that f^n=1 for some n \[Element] \[CapitalZeta] *******)
n=Length[rl];

For[i=1,i<= n,i++,
For[j=i,j<= n,j++,
If[CheckFactor[erl[[i]]/erl[[j]]] && i!=j, letrep=Join[letrep,{erl[[i]]->erl[[j]]}];logrep=Join[logrep,{rl[[i]]->rl[[j]]+Log[Simplify[erl[[i]]/erl[[j]]]]}]];  
erl=(erl//.letrep);
];
];
logs=Union[ rl//.logrep];

If[OptionValue[ShowReplacements],{logs,logrep},logs]
]; 

CPSimplify[e_]:=Module[{rep,e1},e1=e//.{Log[a_*b_]:> Log[a]+Log[b],Log[a_./b_]:> Log[a]-Log[b]};
rep=TPLogariphms[e1,ShowReplacements->True][[2]];
Simplify[e1//.rep]
];
CPSimplify[0]:=0;
(******************************************************************************)
(******************************************************************************)
(************************MPL to canonical**************************************)
CheckWariables[l_List,v_]:=Module[{n=Length[l],i},Table[!FreeQ[l[[i]],v],{i,1,n}]];  

\[Phi][x_,a_+b_]:=\[Phi][x,a]+\[Phi][x,b];    
\[Phi][x_,-SymbolTP[a___]]:=-\[Phi][x,SymbolTP[a]];  
\[Phi][x_,b_*SymbolTP[a___]]:=b*\[Phi][x,SymbolTP[a]];  
\[Phi][x_,SymbolTP[{}]]:=1;
\[Phi][x_,SymbolTP[l_]]:=Module[{check=CheckWariables[l,x],n=Length[l],t}, 
If[ContainsAny[check,{False}],Return[0]]; 
t=Reverse[Table[-((l[[i]]/.x->0)/(D[l[[i]],x])),{i,1,n}]]; 
G[t,x]
];  

\[CapitalPhi][nl_,xl_,a_]:=\[CapitalPhi][nl,xl,Expand[a]];
\[CapitalPhi][nl_,xl_,a_+b_]:=\[CapitalPhi][nl,xl,a]+\[CapitalPhi][nl,xl,b];
\[CapitalPhi][nl_,xl_,-SymbolTP[a___]]:=-\[CapitalPhi][nl,xl,SymbolTP[a]]; 
\[CapitalPhi][nl_,xl_,b_*SymbolTP[a___]]:=b*\[CapitalPhi][nl,xl,SymbolTP[a]];
\[CapitalPhi][nl_,xl_,0]:=0; 
\[CapitalPhi][nl_,xl_,SymbolTP[l_]]:=Module[{m,i,n=Length[xl],j=1},
m=Table[0,{i,1,n}]; 
For[i=1,i<= n,i=i+1,
m[[i]]=l[[j;;j+nl[[i]]-1]];
j=j+nl[[i]];
];
Product[\[Phi][xl[[i]],SymbolTP[m[[i]]]],{i,1,n}]
];

(*CheckSortFor\[CapitalPhi][l1_,l2_,n_]:=Module[{i},
For[i=1,i\[LessEqual]n,i++,
If[l1[[n-i+1]]>l2[[n-i+1]],Return[True]];
If[l1[[n-i+1]]<l2[[n-i+1]],Return[False]];
];
];

GenerateTableFor\[CapitalPhi][n_Integer,k_Integer]:=Module[{innerList,limits,innerTable,l,i,j,h},
innerList=Table[i[j],{j,1,k}];
limits=Table[{i[j],0,n},{j,1,k}];
innerTable=Join[{innerList},limits];
l=Flatten[Table@@innerTable,k-1];
h=Length[l];
For[i=1,i\[LessEqual]h,i++,
If[Plus@@l[[i]]\[NotEqual]n,l=Delete[l,i];h=h-1;i=i-1];
];
Sort[l,CheckSortFor\[CapitalPhi][#1,#2,k]&]
];*)


GenerateTableFor\[CapitalPhi]2[n_Integer,k_Integer]:=Module[{ModifiedInsert},
ModifiedInsert[t_List,x_]:=Insert[t,x,Table[{i,-1},{i,1,Length[t]}]];
If[k==1,Return[{{n}}]];
Join@@Table[ModifiedInsert[GenerateTableFor\[CapitalPhi]2[i,k-1],n-i],{i,0,n}]
];


FindGsFromSymbol[s_,v_List,n_]:=Module[{exPart,ex=0,newS=s,l,k=Length[v],i},
l=GenerateTableFor\[CapitalPhi]2[n,k];
For[i=1,i<=Length[l],i++,
exPart=\[CapitalPhi][l[[i]],Reverse[v],newS];
ex=ex+exPart;
newS=SymbolSimplify[newS-SymbolMPL[exPart]];
(*Print[{exPart,l[[i]],i}]*)
];
ex
]; 

LastElementsToLogs[a_+b_]:=LastElementsToLogs[b]+LastElementsToLogs[a];
LastElementsToLogs[a_*b_]:=LastElementsToLogs[b]*LastElementsToLogs[a];
LastElementsToLogs[a_]:=a/;FreeQ[a,TP];
LastElementsToLogs[TP[l_List]]:=Module[{n=Length[l]},TP[Join[{l[[1]]},Table[Log[Together[LogArg[l[[i]]]]],{i,2,n}]]]];
(*************************************)
FibirnateG1Single[G[{b_},a_],v_]:=G[{b},a]/;FreeQ[b,v]&& FreeQ[a,v];
FibirnateG1Single[G[{b_},a_],v_]:=FibirnateG1Single[G[{0},Together[1-a/b]],v]/;ToString[b]!="0";
FibirnateG1Single[G[{0},a_],v_]:=FibirnateG1Single[G[{0},Collect[a,v]],v];
FibirnateG1Single[G[{0},a_/b_],v_]:=FibirnateG1Single[G[{0},a],v]-FibirnateG1Single[G[{0},b],v];
FibirnateG1Single[G[{0},1/b_],v_]:=-FibirnateG1Single[G[{0},b],v];
FibirnateG1Single[G[{0},a_*b_],v_]:=FibirnateG1Single[G[{0},a],v]+FibirnateG1Single[G[{0},b],v];
FibirnateG1Single[G[{0},b_.*v_],v_]:=G[{0},b]+G[{0},v]/;FreeQ[b,v];
FibirnateG1Single[G[{0},a_.+b_.*v_],v_]:=G[{0},a]+G[{-a/b},v]/;FreeQ[a,v]&&FreeQ[b,v];
FibirnateG1[e_,l_List]:=Module[{},
If[Length[l]==1,Return[e/.{G[{a_},b_]:> FibirnateG1Single[G[{a},b],l[[1]]]}]];
(e/.{G[{a_},b_]:> FibirnateG1Single[G[{a},b],l[[1]]]})/.{G[{a_},b_]/;FreeQ[b,l[[1]]]:> FibirnateG1[G[{a},b],Delete[l,1]]}
];
(*FibirnateG1Single[G[{0},a_],v_]:=Module[{sn,n,ln,cn,i},n=a;
sn=Solve[n==0,v]; 
ln=Length[sn];
If[ln==0 ,Return[G[{0},a]]];
cn=Coefficient[n,v^ln];
G[{0},cn]+G[{0},Product[-(v/.sn[[i]]),{i,1,ln}]] +Sum[G[{v/.sn[[i]]},v],{i,1,ln}]
];*)
(******************************************************************************)
(******************************************************************************************************************************)



End[]
