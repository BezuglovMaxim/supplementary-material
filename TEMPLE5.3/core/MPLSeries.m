(* ::Package:: *)

ep1


Begin["Private`"]


(*ExpandSingleG[G[l_,v_],v_,n_]:=Module[{k=Length[l],Integrand,x,series,seriesIndexes,i,s},
Integrand=Product[1/(x[i]-l[[i]]),{i,1,k}];
seriesIndexes=Table[{x[i],0,n},{i,0,k}];  (*****Redundancy!!!! Need to by fixed****)

series=Normal[Series[Integrand,##] &@@seriesIndexes];

For[i=k,i>=1,i=i-1,
series=GPrimitive[series,x[i]]/.{x[i]->x[i-1]};
];
s=series/.{x[0]->v};
(s/.{v->0})+Sum[Simplify[Coefficient[s,v^i]]v^i,{i,1,n}]
];*)

GSeries[G[l_,arg_],arg_,pover_]:=Module[{a,n,ll=Split[l,##==0 &],j,l3,m,ZZ,afrac,i},
If[ContainsOnly[l,{0}],Return[Log[arg]^Length[l]/Length[l]!]];  
j=Length[ll];a=DeleteCases[l,0];k=Length[a];

ZZ[{},{},n_Integer]:=1;
ZZ[{m_},{x_},n_Integer]:=Sum[x^i/i^m,{i,1,n}];
ZZ[m_List,x_List,n_Integer]:=Sum[x[[1]]^i/i^m[[1]]*ZZ[Delete[m,1],Delete[x,1],i-1],{i,1,n}];

If[!ContainsOnly[ll[[1]],{0}],{ll=Insert[ll,{},1];j=j+1}];
For[i=1,i<=j-1,i++,If[!ContainsOnly[ll[[i]],{0}]&&!ContainsOnly[ll[[i+1]],{0}],{ll=Insert[ll,{},i+1];i=i+1;j=j+1}]];
l3=Drop[ll,{2,Length[ll],2}];
m=Table[Length[l3[[i]]]+1,{i,1,Length[l3]}];
afrac=Table[a[[i]]/a[[i+1]],{i,1,Length[a]-1}];

Collect[(-1)^k Sum[1/n^m[[1]] (arg/a[[1]])^n*ZZ[Delete[m,1],afrac,n-1],{n,1,pover}],arg,Simplify]

];

GSeries[a_,x_,n_]:=Module[{b,l},
b=(RegularizeG[a]/.{G[l_,x]/;!ContainsOnly[l,{0}]:> GSeries[G[l,x],x,n],G[l_,x]/;ContainsOnly[l,{0}]:>Log[x]^Length[l]/Length[l]!});  
Normal[Series[b,{x,0,n}]]
];


NormalizeG[exx_]:=Module[{en},en=RegularizeG[exx];  en/.{G[l_,x_]/;!ContainsOnly[l,{0}]:>G[1/x*l,1]}];  

AdvancedExpandSingleG[G[l_,1],v_,n_]:=Module[{k=Length[l],Integrand,x,series,seriesIndexes,i,s},

If[l[[-1]]==0,Return[Normal[Series[(RegularizeG[G[l,1]]/.{G[ll_,1]:> AdvancedExpandSingleG[G[ll,1],v,n]}),{v,0,n}]]]];


If[FullSimplify[Limit[l[[1]],v->0]]==1,Return[Indeterminate]];
If[ContainsOnly[FullSimplify[Limit[l,v->0]],{0}],Return[Indeterminate]];

Integrand=Product[1/(x[i]-l[[i]]),{i,1,k}];

series=Normal[Series[Integrand,{v,0,n}]];

For[i=k,i>=1,i=i-1,
series=(GPrimitive[series,x[i]]/.{x[i]->x[i-1]})-(GPrimitive[series,x[i]]/.{x[i]->0});
];
s=(series/.{x[0]->1})-(series/.{x[0]->0});
Normal[Series[s,{v,0,n}]]
];

AdvancedExpandMPL[e_,v_,n_]:=Module[{e1},e1=NormalizeG[e];Collect[e1/.{G[ll_,1]:> AdvancedExpandSingleG[G[ll,1],v,n],G[lll_,a_]/;ContainsOnly[lll,{0}]&&ToString[Limit[a,v->0]]!="0" :>Normal[Assuming[ep>0,Series[(Log[a])^Length[lll]/Length[lll]!,{v,0,n}]]] ,G[lll_,a_]/;ContainsOnly[lll,{0}]&&ToString[Limit[a,v->0]]== "0" :>(Log[Normal[Assuming[ep>0,Series[a,{v,0,n+1}]]]])^Length[lll]/Length[lll]! },v]]; 

BoundaryValueGforG[a_+b_,v_]:=BoundaryValueGforG[a,v]+BoundaryValueGforG[b,v];
BoundaryValueGforG[a_*b_,v_]:=BoundaryValueGforG[a,v]*BoundaryValueGforG[b,v];
BoundaryValueGforG[G[l_,0],v_]:=G[l,0];

BoundaryValueGforG[e_,v_]:=Module[{ex,boundry},ex=e/.{v->ep1};
boundry=AdvancedExpandMPL[ex,ep1,0];
If[ToString[boundry]=="Indeterminate" ||boundry==Infinity || boundry==-Infinity || boundry==\!\(\*
TagBox[
RowBox[{
TagBox[
RowBox[{"(", 
RowBox[{"-", "I"}], ")"}],
"DirectedInfinityCoefficient",
Editable->True], " ", "\[Infinity]"}],
DirectedInfinity,
Editable->False]\)|| boundry==\!\(\*
TagBox[
RowBox[{
TagBox["I",
"DirectedInfinityCoefficient",
Editable->True], " ", "\[Infinity]"}],
DirectedInfinity,
Editable->False]\) || !FreeQ[boundry,Infinity] || !FreeQ[boundry,-Infinity] , Return[((NormalizeG[ex]//.{G[ll_,1]/;(Simplify[Limit[ll[[1]],ep1->0]])!=1&&!ContainsOnly[Simplify[Limit[ll,ep1->0]],{0}]:> BoundaryValueGforG[G[ll,1],ep1],G[ll_,1]/;(Simplify[Limit[ll[[1]],ep1->0]])== 1:> G[Normal[Series[ll,{ep1,0,1}]],1],G[ll_,1]:> G[Normal[Series[ll,{ep1,0,1}]],1],G[l_,0]:>G[l,0] })/.{Log[a22_] :> G[{0},a22]})/.{ep1->ep}]]; 
boundry/.{Log[a22_] :> G[{0},a22]}/.{ep1->ep}
];


End[]
