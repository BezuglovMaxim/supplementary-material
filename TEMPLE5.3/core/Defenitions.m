(* ::Package:: *)

DE4::usage="derivative"


NMPL::usage="Numerical evaluation of MPLs"


GSeries::usage="Series Expansions of MPLs"


ExpandEMPLs::usage="Series Expansions of EMPLs"


ySer::usage="Expand y as series"


BrownReduction::usage="The Fubini Reduction algorithm"


E4Primitive::usage="Primitive of E4 functions and ther cernels"


ModifiedApart::usage="rewrites a rational expression as a sum of terms with minimal denominators, But batter then usual apart"


FindListOfE4s::usage="find list of all E4 in the expression"


E4Integrate::usage="Integrate a linear combination of E4 with rational coefficients"


RewriteAllGs::usage="Rewrites all MPLs in the expression as EMPLs"


ep::usage="Reguralization"


FullSimplifyE4Coefficients::usage="Regurion"


SimplifyE4Coefficients::usage="Regurion"


E4Splitt::usage="Splitter"


G::usage="MPL"


IG::usage="Other MPL"


GPrimitive::usage="Primitive for MPLs "


FindListOfGs::usage="Find list of MPLs "


GSplitt::usage="Find list of MPLs "


DG::usage="Derivative of MPLs "


GsAsIGs::usage="Rewrite all Gs as IGs"


IGsAsGs::usage="Rewrite all IGs as Gs"


RewriteAllGsAsG::usage="Rewrite all IGs as Gs"


ExpandMPLs::usage="Series Expansions of MPLs in x, simple case when in G[{a1,a2,...an},x] ai dose not depend on x"


AdvancedExpandMPL::usage="Series Expansions of MPLs in x, more complicated case"


BoundaryValueGforG::usage="BoundaryValue"


NEvaluateE4::usage="Numericly evaluate expression with elliptic functions"


CheckPerfection::usage="Check wether curve is perfect"


ProgectiveHyperserface::usage="Bilds projective serfase"


RationalizationNotInfinity::usage="Rationalize root wich perfect points are not in infinity"


ER4::usage="Regularized E4 function"


E4Visualize::usage="Visualization of E4 function"


ShuffleAlgebra::usage="Gives a shuffle product of two lists"


ShuffleSimplify::usage="Simplifies the expresion with E4 using shuffle algebra"


RegularizeG::usage="Regularize all G"


RegularizeE4::usage="Regularize all E4"


E4Reg::usage="Put regularized wersion for E4"


GReg::usage="Put regularized wersion for G"


X


SplittSumConditionaly::usage="s"


Symanzik::usage="gives a list {U,F} of two Symanzik polinomials"


SplittSumConditionaly::usage="s"


BezuTheorem::usage="s"


SimplifyGCoefficients::usage="Simplify G Coefficients"


NormalizeG::usage="Normalizes all Gs"


HolderConvolution::usage="HolderConvolution"


E4SplittGCoefficients::usage=""


GCoproduct::usage="Coproduct of MPLs"


GCoproduct::usage="Coproduct of MPLs"


SymbolMPL::usage="Symbol of MPLs"


FindGsFromSymbol::usage="Find MPLs with given symbol and given order of variables"


TP::usage="Tensor Product"


SymbolTP::usage="Symbol Tensor Product"


SymbolSimplify::usage="Simplifyey Symbol Tensor Product"


FindEquationForRoot


SymbolLetters


SimplifyArgOfTF::usage="Simplify transcendental arguments"


GasGListOfElements::usage=""


GasGListOfReplacments::usage=""


RegularizeGInAPoint::usage=""
