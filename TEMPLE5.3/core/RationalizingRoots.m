(* ::Package:: *)

t


Begin["Private`"]



DModified[l_List,v_List]:=Flatten[Table[D[l[[i]],v[[j]]],{i,1,Length[l]},{j,1,Length[v]}]];

PolinomDegre[f_,v_List]:=Module[{i,PD,l1,cc},PD[a_]:=PD[Expand[a]];PD[a_+b_]:=Join[PD[a],PD[b]];PD[a_]:={a}; 
l1=PD[f]/.Table[v[[i]]-> cc,{i,1,Length[v]}];
Max[Table[Exponent[l1[[i]],cc],{i,1,Length[l1]}]]
];
ProgectiveHyperserface[f_,v_List,z_]:=Module[{reprules=Table[v[[i]]->v[[i]]/z,{i,1,Length[v]}]},Expand[z^PolinomDegre[f,v] (f/.reprules)]]; 
CreateRepRules[l_,v_]:=Thread[v->l];  

(*****************************************************************************************************************************)
(*****************************************************************************************************************************)
(*****************************************************************************************************************************)

CheckPerfection[f_,v_List]:=Module[{ff,s,l,fList,SystemSolutionsRow,SystemSolutions,r,j,d=PolinomDegre[f,v],PerfectPoints={}},
Off[Solve::svars];
If[!IrreduciblePolynomialQ[f],Return[{False}]];
If[PolinomDegre[f,v]== 2, Return[{True,v/.Solve[f==0,v[[1]]]}]];
(************************First part, finds points of the curve***************************************************)
fList[0]={f};
SystemSolutionsRow[0]=Solve[f==0,v];
SystemSolutions[0]=Union[Flatten[Table[v/.SystemSolutionsRow[0],{i,1,Length[SystemSolutionsRow[0]]}],1]];
(************************************************************************************************************)
(************************Second part, finds all points where all derivetives of order < d-1 is zero***********) 
For[r=1,r<d-1,r++,
fList[r]=DModified[fList[r-1],v];

For[j=1,j<= Length[SystemSolutions[r-1]],j++,

SystemSolutionsRow[r][j]=Solve[Table[(fList[r][[i]]/.CreateRepRules[SystemSolutions[r-1][[j]],v])==0,{i,1,Length[fList[r]]}],v];
SystemSolutions[r][j]=Union[Flatten[Table[SystemSolutions[r-1][[j]]/.SystemSolutionsRow[r][j],{i,1,Length[SystemSolutionsRow[r][j]]}],1]];

];
SystemSolutions[r]=Union@@Table[SystemSolutions[r][i],{i,1,Length[SystemSolutions[r-1]]}];
];
(*************************************************************************************************************)

(*****************************************************************)
fList[r]=DModified[fList[r-1],v];
For[i=1,i<=Length[SystemSolutions[r-1]],i++,
If[Union[fList[r]/.CreateRepRules[SystemSolutions[r-1][[i]],v]]!={0},PerfectPoints=Join[PerfectPoints,{SystemSolutions[r-1][[i]]},1]];
];
(*****************************************************************)
(******************Part wich cheackse spesial cases, in case the points depend on elements of v*****************)
ff[{x_->y_}]:={x!=y};
       ff[{a_}]:={ff[a]};
       ff[{a___,b_}]:=Flatten[{ff[{a}],ff[b]}]; 

For[i=1,i<= Length[PerfectPoints],i++,
s=Solve[Table[(fList[r][[j]]/.CreateRepRules[PerfectPoints[[i]],v])==0,{j,1,Length[fList[r]]}],v];
If[s!= {},PerfectPoints[[i]]={PerfectPoints[[i]],ff[s]}]
];
(**************************************************************************************************************)

On[Solve::svars];
If[PerfectPoints=={},{False},{True,Union[PerfectPoints]}]
];

SplitInToHomogeniusParts[g_,v_]:=Module[{z,gd,gdm1,d=PolinomDegre[g,v]},
 gd=Simplify[z^d*g/.Thread[v-> v/z ]]/.z->0;
gdm1=FullSimplify[g-gd];
{gdm1,gd}];


RationalizationNotInfinity[f_,v_,p_]:=Module[{hp,rat},hp=SplitInToHomogeniusParts[f/.Table[v[[i]]->v[[i]]+p[[i]],{i,1,Length[v]}],v];
rat=-(hp[[1]]/hp[[2]])/.Table[v[[i]]->t[i-1],{i,1,Length[v]}];
t[0]:=1;
FullSimplify[Together[Table[v[[i]]->t[i-1]rat+p[[i]],{i,1,Length[v]}]]]
];


(****************************************************************************************************************************************)
ConsistAnyFromListQ[e_,v_]:=Module[{i},For[i=1,i<=Length[v],i++,If[!FreeQ[ e,v[[i]] ],Return[True]]]; False];
RepPowersWihRoots[x_,v_:{}]:=x/.{a_^n_./;Denominator[n]!= 1 &&(ConsistAnyFromListQ[a,v]|| v== {})  :>a^Floor[n] root[Denominator[n],a^Numerator[n-Floor[n]]]};
(****************************************************************************************************************************************)
RootSimplify[a_,rec_:1]:=RootSimplify[Expand[a],rec+1];
RootSimplify[a_+b_,rec_:1]:=RootSimplify[a,rec+1]+RootSimplify[b,rec+1];
RootSimplify[a_,rec_:1]:=a/;FreeQ[a,root];
RootSimplify[root[a_,b_],rec_:1]:=root[a,b];
RootSimplify[a_*b_,rec_:1]:=a*RootSimplify[b,rec+1]/;FreeQ[a,root];
RootSimplify[c_.*root[n_.,a_.]^k_.,rec_:1]:=RootSimplify[c*   a^Floor[k/n] root[Denominator[k/n],a]^Numerator[k/n-Floor[k/n]],rec+1]/;k!=1 && k>= n;
RootSimplify[c_,n_]:=c/;n>300;
RootSimplify[b_.*root[n1_,a_^k1_.]^l1_. root[n2_,a_^k2_.]^l2_.,rec_:1]:=RootSimplify[b* a^Floor[l1 k1/n1+l2 k2/n2] root[Denominator[l1 k1/n1+l2 k2/n2],a]^Numerator[l1 k1/n1+l2 k2/n2-Floor[l1 k1/n1+l2 k2/n2]],rec+1];(**?????? Check last line, It can be incorrect in some particular cases!!!!!!!!!!!!!!!!!!!!**)
(*-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-*)
root[1,1]:=1;
(*-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-**-*-*-*)
(****************************************************************************************************************************************)
FindListOfRoots[e_]:=Block[{$RecursionLimit=120000,flor}, 
flor[a_+b_]:=Union[Join[flor[a],flor[b]]];
flor[root[a_,b_]]:={root[a,b]};flor[c_]:={}/; FreeQ[c,root];
flor[a_]:=flor[Expand[a]];flor[root[a_,b_]v_]:=flor[root[a,b]+v];
flor[a_./c_.]:=flor[c+a]; 
flor[f_[a_]]:=flor[a];flor[b_.*c_^a_.]:=flor[b+a+c];
flor[e]];  
(****************************************************************************************************************************************)

FindRationalEquationForRoot[r_,y_,v_:{}]:=Module[{f=r-y,rootList,s,RootDegree,d},
f=RootSimplify[RepPowersWihRoots[f,v]];
RootDegree[root[a_,b_]]:=a;

While[!FreeQ[f,root],
rootList=FindListOfRoots[f];
s=Solve[f==0,rootList[[1]]];
d=RootDegree[rootList[[1]]];
f=RootSimplify[(rootList[[1]]/.s[[1]])^d-rootList[[1]]^d];
];
If[FreeQ[RepPowersWihRoots[f,v],root],Return[f],FindRationalEquationForRoot[f,0,v]]
];
(***************************************************************************************************************************************)

 FindEquationForRoot[r_,y_,v_:{}]:=Module[{f},
f=FindRationalEquationForRoot[r,y,v];
f=FullSimplify[f];
f=Numerator[Together[f]];
f
];
(*****************************************************************************************************************************************)
(*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*)
(***********************                                                                                  **********************************)
(***********************                                                                                  **********************************)
(*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*)
(*****************************************************************************************************************************************)




End[]
