(* ::Package:: *)

SplitRational


LinearCoefficientsOfE4s


ToEMPLCernelsRational


FindListOfZ4s


ModifiedApart
MAstage


BoundaryValueGforE4


Begin["Private`"]


E4Primitive[expr_,variable_,RepRules_:{},OptionsPattern[{EllipticCurve->y,BranchPointsLetter->a,VAssumptions ->{}}]]:=Monitor[Block[{$IterationLimit=40000,$RecursionLimit=40000,E4prim,yi=OptionValue[EllipticCurve],ai=OptionValue[BranchPointsLetter],ass=OptionValue[VAssumptions],FFIBP,q1,q2,q3,Fi4,f1,f2,templeInner},
(****************Basics*******************) 
E4prim[a_ + b_, x_] := E4prim[a,x] +E4prim[b, x]; 
E4prim[c_ f_, x_] := c E4prim[f, x] /; FreeQ[c, x] && FreeQ[c/.{yi[bc_]:> templeInner,1/yi[bc_]:>  templeInner},yi];

E4prim[G[c___]*f_, x_] :=G[c] E4prim[f, x] /; FreeQ[{c}, x]; 
E4prim[G[c___]*1/f_, x_] :=G[c] E4prim[1/f, x] /; FreeQ[{c}, x]; 

(*E4prim[a_./(n_.*x_+c_.+g_.*x_),x_]:=E4prim[a/((n+g)*x+c),x];   (**********28.08.2019*****************)
E4prim[a_./(n_+(x_+c_)*g_),x_]:=E4prim[a/(n+g*c+g*x),x];     (**********30.08.2019*****************)
E4prim[a_./((b_.*yi+c_.*yi)+x_*(d_.*yi+e_.*yi)),x_]:=E4prim[a/(yi((b+c)+x*(d+e))),x];     (**********30.08.2019*****************)

E4prim[g_./(a_.+b_.(c_.+d_.*x_)+f_.*x_),x_]:=E4prim[g/(a+b*c+(b*d+f)x),x];     (**********17.10.2019*****************)
E4prim[g_./(a_.+b_.(c_.+d_.*x_)+f_.(l_.+k_.*x_)),x_]:=E4prim[g/(a+b*c+f*l+(b*d+f*k)x),x];     (**********17.10.2019*****************)
E4prim[g_./(a_.+b_.*yi+c_.*yi),x_]:=E4prim[g/(a+(b+c)yi),x];     (**********17.10.2019*****************)
E4prim[g_./(a_.*yi+b_.*yi),x_]:=E4prim[g/((a+b)*yi),x];     (**********17.10.2019*****************)*)

E4prim[yi[c_]^n_. f_, x_] :=yi[c]^n E4prim[f, x] /; FreeQ[c, x];
E4prim[a_./(b_.*c_+d_.*c_),x_]:=1/(b+d) E4prim[a/c, x] /; FreeQ[{b,d}, x] && FreeQ[{b,d}, yi];
(****************EMPLs********************)
(***********************zero********************)
 E4prim[1/yi,x_]:=\[Omega]1[ai]/c4[ai] E4[{{0,0}},x,ai];
 E4prim[1/yi E4[l_,x_,ai],x_]:=\[Omega]1[ai]/c4[ai] E4[Insert[l,{0,0},1],x,ai];
(***********************one********************)
 E4prim[Z4[x_,ai]/yi,x_]:=-1/c4[ai] E4[{{1,Infinity}},x,ai];
 E4prim[Z4[x_,ai]/yi E4[l_,x_,ai],x_]:=-1/c4[ai] E4[Insert[l,{1,Infinity},1],x,ai];
 E4prim[x_/yi,x_]:=E4[{{-1,Infinity}},x,ai]+E4prim[1/yi (ai[1]+2c4[ai] Gstar[ai]),x];
 E4prim[x_/yi E4[l_,x_,ai],x_]:=E4[Insert[l,{-1,Infinity},1],x,ai]+E4prim[1/yi (ai[1]+2c4[ai] Gstar[ai])E4[l,x,ai],x];
 
 E4prim[1/(n_.*x_+c_.),x_]:=1/n E4[{{1,-c/n}},x,ai]/; FreeQ[{n,c}, x] && FreeQ[{n,c}, yi];
 E4prim[1/(n_.*x_+c_.) E4[l_,x_,ai],x_]:=1/n E4[Insert[l,{1,-c/n},1],x,ai]/; FreeQ[{n,c}, x]&& FreeQ[{n,c}, yi];

 E4prim[1/(yi(n_.*x_+c_.)),x_]:=1/(n yi[-c/n]) (E4[{{-1,-c/n}},x,ai]-E4prim[Z4[-c/n,ai] c4[ai]/yi,x])/;FreeQ[c,ai]&& FreeQ[{n,c}, x]&& FreeQ[{n,c}, yi]; 
 E4prim[E4[l_,x_,ai]/(yi(n_.*x_+c_.)),x_]:=1/(n yi[-c/n]) (E4[Insert[l,{-1,-c/n},1],x,ai]-E4prim[Z4[-c/n,ai] c4[ai]/yi E4[l,x,ai],x])/;FreeQ[c,ai]&& FreeQ[{n,c}, x]&& FreeQ[{n,c}, yi]; 
(***********************two********************)
E4prim[(x_*Z4[x_,ai])/yi,x_]:=-2/ \[Omega]1[ai] E4[{{-2,Infinity}},x,ai]+
E4prim[1/c4[ai]+2 c4[ai]/yi Z4[x,ai](ai[1]/(2c4[ai])+Gstar[ai]) ,x];

 E4prim[(x_*Z4[x_,ai])/yi E4[l_,x_,ai],x_]:=-2/ \[Omega]1[ai] E4[Insert[l,{-2,Infinity},1],x,ai]+
 E4prim[(1/c4[ai]+2 c4[ai]/yi Z4[x,ai](ai[1]/(2c4[ai])+Gstar[ai]))E4[l,x,ai] ,x];
(***************)
E4prim[Z4[x_,ai]/(yi(x_+c_.)),x_]:=-2/(yi[-c]\[Omega]1[ai]) E4[{{-2,-c}},x,ai]-
(c4[ai]Z4[-c,ai])/yi[-c] E4prim[Z4[x,ai]/yi,x]+Z4[-c,ai]/yi[-c] E4prim[1/(x+c),x];

E4prim[Z4[x_,ai]/(yi(x_+c_.)) E4[l_,x_,ai],x_]:=-2/(yi[-c]\[Omega]1[ai]) E4[Insert[l,{-2,-c},1],x,ai]-
(c4[ai]Z4[-c,ai])/yi[-c] E4prim[Z4[x,ai]/yi E4[l,x,ai],x]+Z4[-c,ai]/yi[-c] E4prim[1/(x+c) E4[l,x,ai],x];
(***************)
E4prim[Z4[x_,ai]/(x_+c_.),x_]:=2/\[Omega]1[ai] (-E4[{{2,-c}},x,ai]+E4[{{2,Infinity}},x,ai])+E4prim[Fi4[x],x]+2((2c4[ai] (-c-ai[1])  )/((ai[1]-ai[3])(ai[2]-ai[4]))-q1 c4[ai])E4prim[x/yi,x] +
2(q2[-c]-q1)c4[ai]E4prim[1/yi,x]+Z4[-c,ai]yi[-c]E4prim[1/(yi(x+c)),x]; 

E4prim[Z4[x_,ai]/(x_+c_.) E4[l_,x_,ai],x_]:=2/\[Omega]1[ai] (-E4[Insert[l,{2,-c},1],x,ai]+E4[Insert[l,{2,Infinity},1],x,ai])+E4prim[Fi4[x]E4[l,x,ai],x]+2((2c4[ai] (-c-a[1]) )/((ai[1]-ai[3])(ai[2]-ai[4]))-q1 c4[ai])E4prim[x/yi E4[l,x,ai],x]
 +2(q2[-c]-q1)c4[ai]E4prim[1/yi E4[l,x,ai],x]+Z4[-c,ai]yi[-c]E4prim[1/(yi(x+c)) E4[l,x,ai],x];
(***************)
E4prim[Z4[x_,ai]^2/yi,x_]:=4/(c4[ai]\[Omega]1[ai]) E4[{{2,Infinity}},x,ai]-E4prim[8/yi f1[x]Z4[x,ai],x]-E4prim[8/yi f2[x],x]-2/c4[ai] E4prim[(Z4[x,ai]/(x-ai[1])-Fi4[x]),x]-E4prim[4q1 1/yi  ,x]+E4prim[(4q3)/c4[ai] x/yi   ,x];

E4prim[Z4[x_,ai]^2/yi E4[l_,x_,ai],x_]:=4/(c4[ai]\[Omega]1[ai]) E4[Insert[l,{2,Infinity},1],x,ai]-E4prim[8/yi (f1[x]Z4[x,ai]+f2[x])E4[l,x,ai],x]-2/c4[ai] E4prim[(Z4[x,ai]/(x-ai[1])-Fi4[x])E4[l,x,ai],x]-
E4prim[4q1 1/yi E4[l,x,ai] ,x]+E4prim[(4q3)/c4[ai] x/yi E4[l,x,ai]   ,x];

(********************************Abbreviations********************************************)
q1:=(4ai[1]c4)/((ai[1]-ai[3])(ai[2]-ai[4])) Gstar[ai]+Gstar[ai]^2+2 \[Eta]1[ai]/\[Omega]1[ai]+(3ai[1]^2+(ai[2]+ai[3]+ai[4])ai[1]-2(ai[3]ai[4]+ai[2]ai[3]+ai[2]ai[4]))/((ai[1]-ai[3])(ai[2]-ai[4]));

q2[c_]:=1/4 Z4[c,ai]^2+((ai[1]-c)(ai[2]+ai[3]+ai[4]-c))/((ai[1]-ai[3])(ai[2]-ai[4]));
q3:=Gstar[ai]+(3ai[1]-ai[2]-ai[3]-ai[4])/(4c4[ai]); 
 Fi4[x_]:=(1/(c4[ai]*yi) (x^2-SymmetricPolynomial[1,Table[ai[i],{i,1,4}]]/2 x+ SymmetricPolynomial[2,Table[ai[i],{i,1,4}]]/6)+4c4[ai]*\[Eta]1[ai]/\[Omega]1[ai] 1/yi);

f1[x_]:=((ai[2]-x)(ai[3]-x)(ai[4]-x))/(c4[ai] x);  
f2[x_]:=(-3ai[3]x-3ai[4]x-ai[1](ai[2]+ai[3]+ai[4]-3x)+ai[2](2ai[3]+2ai[4]-3x)+2ai[3]ai[4]+3x^2)/(6(ai[1]-ai[3])(ai[2]-ai[4])); 
FFIBP[i_ /;i>=1 && i<=4]:=Module[{l},l=Delete[{1,2,3,4},i];Product[ai[i]-ai[l[[j]]],{j,1,3}]]; (*FactorForIntegrationByParts*)

(****************Integration by parts***********************************)

 (*E4prim[a_./(yi(x_-ai[i_])),x_]:=1/FFIBP[i] ((2yi a)/(ai[i]-x)+2E4prim[yi/(x-ai[i]) DE4[a,x,BranchPointsLetter->ai,EllipticCurve->yi],x]+
 (4ai[i]-Sum[ai[j],{j,1,4}])E4prim[(x-ai[i])/yi a,x]+2E4prim[(x-ai[i])^2/yi a,x]);  

 E4prim[x_^2/yi Z4[x_,ai],x_]:=E4prim[(-1)Z4[x,ai] c4[ai](1/(c4[ai]yi) (-(SymmetricPolynomial[1,Table[ai[i],{i,1,4}]]/2)x+ 
 SymmetricPolynomial[2,Table[ai[i],{i,1,4}]]/6)+4c4[ai]*\[Eta]1[ai]/\[Omega]1[ai] 1/yi),x]+c4[ai]/2 Z4[x,ai]^2;

 E4prim[x_^2/yi a_.,x_]:=E4prim[(-1)a c4[ai](1/(c4[ai]*yi) (-(SymmetricPolynomial[1,Table[ai[i],{i,1,4}]]/2)x+ 
 SymmetricPolynomial[2,Table[ai[i],{i,1,4}]]/6)+4c4[ai]*(\[Eta]1[ai]/\[Omega]1[ai])*(1/yi)),x]+
c4[ai]*Z4[x,ai]a-c4[ai]E4prim[Z4[x,ai]DE4[a,x,BranchPointsLetter->ai,EllipticCurve->yi],x]; *)

E4prim[1/(yi(x_-ai[i_])),x_]:=1/FFIBP[i] ((2yi)/(ai[i]-x)+
 (4ai[i]-Sum[ai[j],{j,1,4}])E4prim[(x-ai[i])/yi,x]+2E4prim[(x-ai[i])^2/yi,x]);
 
 E4prim[E4[l_,x_,ai]/(yi(x_-ai[i_])),x_]:=1/FFIBP[i] ((2yi E4[l,x,ai])/(ai[i]-x)+2E4prim[yi/(x-ai[i]) DE4[E4[l,x,ai],x,BranchPointsLetter->ai,EllipticCurve->yi],x]+
 (4ai[i]-Sum[ai[j],{j,1,4}])E4prim[(x-ai[i])/yi E4[l,x,ai],x]+2E4prim[(x-ai[i])^2/yi E4[l,x,ai],x]);  

 E4prim[x_^2/yi Z4[x_,ai],x_]:=E4prim[(-1)Z4[x,ai] c4[ai](1/(c4[ai]yi) (-(SymmetricPolynomial[1,Table[ai[i],{i,1,4}]]/2)x+ 
 SymmetricPolynomial[2,Table[ai[i],{i,1,4}]]/6)+4c4[ai]*\[Eta]1[ai]/\[Omega]1[ai] 1/yi),x]+c4[ai]/2 Z4[x,ai]^2; 
 
E4prim[x_^2/yi,x_]:=E4prim[(-1)c4[ai](1/(c4[ai]*yi) (-(SymmetricPolynomial[1,Table[ai[i],{i,1,4}]]/2)x+ 
 SymmetricPolynomial[2,Table[ai[i],{i,1,4}]]/6)+4c4[ai]*(\[Eta]1[ai]/\[Omega]1[ai])*(1/yi)),x]+
c4[ai]*Z4[x,ai]; 

 E4prim[x_^2/yi E4[l_,x_,ai],x_]:=E4prim[(-1)E4[l,x,ai] c4[ai](1/(c4[ai]*yi) (-(SymmetricPolynomial[1,Table[ai[i],{i,1,4}]]/2)x+ 
 SymmetricPolynomial[2,Table[ai[i],{i,1,4}]]/6)+4c4[ai]*(\[Eta]1[ai]/\[Omega]1[ai])*(1/yi)),x]+
c4[ai]*Z4[x,ai]E4[l,x,ai]-c4[ai]E4prim[Z4[x,ai]DE4[E4[l,x,ai],x,BranchPointsLetter->ai,EllipticCurve->yi],x]; 

(*************************************)

E4prim[E4[l_,x_,ai],x_]:=(x*E4[l,x,ai]- E4prim[x*DE4[E4[l,x,ai],x,BranchPointsLetter->ai,EllipticCurve->yi],x]);
E4prim[x_^n_.*E4[l_,x_,ai],x_]:=(x^(n+1)*E4[l,x,ai])/(n+1 ) -1/(n+1) *E4prim[x^(n+1)*DE4[ E4[l,x,ai],x,BranchPointsLetter->ai,EllipticCurve->yi],x]/;n!= -1; 
E4prim[(x_+a_.)^n_.*E4[l_,x_,ai],x_]:=((x+a)^(n+1)*E4[l,x,ai])/(n+1 ) -1/(n+1) *E4prim[(x+a)^(n+1)*DE4[ E4[l,x,ai],x,BranchPointsLetter->ai,EllipticCurve->yi],x]/;n!= -1&& FreeQ[a, x]&&FreeQ[a,yi];

(*19.09.2019*)
E4prim[x_^n_./yi,x_]:=1/(n+1) (x^(n+1)/yi+1/2 E4prim[E4Splitt[x^(n+1)/yi Sum[1/(x-ai[i]),{i,1,4}],RepRules,BranchPointsLetter->ai,Variable->x,EllipticCurve->yi,VAssumptions ->ass],x])/;n<= -2;
E4prim[(x_+c_.)^n_./yi,x_]:=1/(n+1) ((x+c)^(n+1)/yi+1/2 E4prim[E4Splitt[(x+c)^(n+1)/yi Sum[1/(x-ai[i]),{i,1,4}],RepRules,BranchPointsLetter->ai,Variable->x,EllipticCurve->yi,VAssumptions ->ass],x])/;n<= -2; 

E4prim[(x_+c_.)^n_./yi*E4[l_,x_,ai],x_]:=1/(n+1) ((x+c)^(n+1)/yi E4[l,x,ai]-E4prim[(x+c)^(n+1)/yi*DE4[E4[l,x,ai],x,BranchPointsLetter->ai,EllipticCurve->yi],x]+1/2 E4prim[E4Splitt[(x+c)^(n+1)/yi*Sum[1/(x-ai[i]),{i,1,4}]*E4[l,x,ai],RepRules,BranchPointsLetter->ai,Variable->x,EllipticCurve->yi,VAssumptions ->ass],x])/;n<= -2; 
E4prim[(x_)^n_./yi*E4[l_,x_,ai],x_]:=1/(n+1) ((x)^(n+1)/yi E4[l,x,ai]-E4prim[(x)^(n+1)/yi*DE4[E4[l,x,ai],x,BranchPointsLetter->ai,EllipticCurve->yi],x]+1/2 E4prim[E4Splitt[(x)^(n+1)/yi*Sum[1/(x-ai[i]),{i,1,4}]*E4[l,x,ai],RepRules,BranchPointsLetter->ai,Variable->x,EllipticCurve->yi,VAssumptions ->ass],x])/;n<= -2; 
(*end of 19.09.2019*)
(****************Simple rules*************)
 E4prim[c_, t_] := c t /; FreeQ[c, t]&&FreeQ[c,yi];
        E4prim[t_^n_., t_] := 1/(n + 1) t^(n + 1) /; n != -1;
        E4prim[(u_ + t_)^n_., t_] := 1/(n + 1) ((u + t)^(n + 1) ) /; (n != -1) && FreeQ[u, t]&&FreeQ[u,yi];
        E4prim[(u_ + v_*t_)^n_., t_] := 1/(n + 1)/v ((u + v*t)^(n + 1)) /; (n != -1) && FreeQ[{u, v}, t]&&FreeQ[{u,v},yi];
        E4prim[(u_.*t_ + v_ + w_. t_)^n_., t_] := E4prim[((u + w)*t + v)^n,t] /; (n != -1) && FreeQ[{u, v, w}, t]&&FreeQ[{u,v,w},yi];
 E4prim[Exp[c_ *t_],t_]:=Exp[c *t]/c/; FreeQ[c, t];
 
 (****************E4Splitt************************)
E4prim[a_,x_]:=E4prim[E4Splitt[a,RepRules,BranchPointsLetter->ai,Variable->x,EllipticCurve->yi,VAssumptions ->ass],x];
 (************************************************)

 
E4prim[E4Splitt[expr,RepRules,BranchPointsLetter->ai,Variable->variable,EllipticCurve->yi,VAssumptions ->ass],variable]
],Row[{ProgressIndicator[Appearance->"Necklace"],"Primitive"}," "]];
(*Primitive of E4 functions and ther cernels, contains some simple cases*)
(***************************************************************************************************************************************************************************************)
(***************************************************************************************************************************************************************************************)
(***************************************************************************************************************************************************************************************)
SplitRational[r_,RepRules_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,BranchPointsLetter->a}]]:=
Monitor[Module[{s,R0,N0,D0,Sn,Sd,Qn,Qd,P4,ReplacmentForElipticCurve,xi=OptionValue[Variable],yi=OptionValue[EllipticCurve],ai=OptionValue[BranchPointsLetter]},
ReplacmentForElipticCurve[n_]:=If[EvenQ[n],(Product[(xi-ai[i]),{i,1,4}])^(n/2),yi (Product[(xi-ai[i]),{i,1,4}])^((n-1)/2)] ;
R0=ReleaseHold[ExpandDenominator[ExpandNumerator[Together[r]]]/.{yi^n_->Hold[ReplacmentForElipticCurve[n]]}]; 
N0=Numerator[R0];
D0=Denominator[R0];
Sn=N0/.{yi->0,yi[s_]->yi[s]} ;   Sd=D0/.{yi->0,yi[s_]->yi[s]}; Qn=D[N0,yi];   Qd=D[D0,yi]; P4=Product[(xi-ai[i]),{i,1,4}];
(*{R1->Together[Simplify[((Sn Sd-P4 Qn Qd)/(Sd^2-P4 Qd^2))/.RepRules]], R2-> Together[1/yi Simplify[((P4(Qn Sd-Sn Qd))/(Sd^2-P4 Qd^2))/.RepRules]]}*)
{R1->Together[((Sn Sd-P4 Qn Qd)/(Sd^2-P4 Qd^2))/.RepRules], R2-> Together[1/yi ((P4(Qn Sd-Sn Qd))/(Sd^2-P4 Qd^2))/.RepRules]}
],Row[{ProgressIndicator[Appearance->"Necklace"],"SplitRational"}," "]];(*Revrite rational function(defined R) R(x,y) as R1(x)+R2(x,y), where R2(x,y)=1/yR3(x)*)    

ZeroList[polinomial_,x_:x]:=Monitor[Module[{i,s},
s=Solve[polinomial==0,x];Union[Table[x/.s[[i]]],{i,1,Length[s]}]]
,Row[{ProgressIndicator[Appearance->"Necklace"],"ZeroList"}," "]]; (*find a list of zerous of the polinomial*)

ModifiedApart[rational_,x_:x,RepRules_List:{},OptionsPattern[{BranchPointsLetter->a,VAssumptions ->{},Checkpoles->True}]]:=  
Monitor[Module[{PolesExtractor,zl,d,rem,poles,i,apoles=OptionValue[BranchPointsLetter],ass=OptionValue[VAssumptions]},MAstage=1; d=Denominator[rational];
zl=Simplify[ZeroList[d,x]]; 
MAstage=2;
poles=Sum[Normal[Series[rational,{x,zl[[i]],-1}]],{i,1,Length[zl]}]+Normal[Series[rational,{x,Infinity,0}]];
MAstage=3;
PolesExtractor[a_+b_]:=PolesExtractor[a]+PolesExtractor[b]; 
PolesExtractor[a_]:=a/;FreeQ[a,x];
PolesExtractor[a_*b_]:=a*PolesExtractor[b]/;FreeQ[a,x];
PolesExtractor[(a_.+x)^n_.]:=(PolesExtractor[1/(a+x)])^-n/;n<-1; 
PolesExtractor[x^n_.]:=x^n;
PolesExtractor[1/(a_.+x)]:=CheckPoles[1/(x+a),x,RepRules,BranchPointsLetter->apoles,VAssumptions -> ass]/;FreeQ[a,x]; 
MAstage=4;
(*rem=FullSimplify[rational-poles];*)
MAstage=5;
(*PolesExtractor[poles+FullSimplify[rational-poles]]*)
If[OptionValue[Checkpoles],PolesExtractor[poles],poles]
(*PolesExtractor[poles]*)
(*poles*)
],Row[{ProgressIndicator[Appearance->"Necklace"],"ModifiedApart,","  stage:", MAstage}," "]]; (*rewrites a rational expression as a sum of terms with minimal denominators, But batter then usual apart, it works even when the denominator can not be factorized, in some sens it is Lorane seriese*)



CheckPoles[1/(x_+a_),x_,RepRules_List:{},OptionsPattern[{BranchPointsLetter->a,VAssumptions ->{}}]]:=
Monitor[Module[{d,i,apoles=OptionValue[BranchPointsLetter],ass=OptionValue[VAssumptions]},
For[i=1,i<=4,i++, 
If[ass=={},If[FullSimplify[(a+apoles[i])/.RepRules]==0,Return[1/(x-apoles[i])]],
If[(Assuming[##,FullSimplify[(a+apoles[i])/.RepRules]]&@@ ass)==0, Return[1/(x-apoles[i])]]]
];    
1/(x+a)
],Row[{ProgressIndicator[Appearance->"Necklace"],"CheckPoles"}," "]]; (*Chek if the pole is in the branch point of the elliptic cureve, if true rewrites it in the form 1/(x-a[i]), where a[i] -is the pole *)



 ToEMPLCernelsRational[RationalFunction_,RepRules_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,BranchPointsLetter->a,VAssumptions ->{}}]]:= 
Monitor[Module[{i,RZCoef,SimplifyParts,RRep12,Rr1,Rr2,poles1,R12,Rules,xi=OptionValue[Variable],yi=OptionValue[EllipticCurve],apoles=OptionValue[BranchPointsLetter],ass=OptionValue[VAssumptions]},

RZCoef=CoefficientList[RationalFunction,Z4[xi,apoles]];

For[i=1,i<=Length[RZCoef],i++,
RRep12[i]=SplitRational[RZCoef[[i]],RepRules,BranchPointsLetter->apoles,Variable->xi,EllipticCurve->yi]; 
Rr1[i]=(R1/.RRep12[i]); Rr2[i]=(R2/.RRep12[i]); 
R12[i]=ModifiedApart[Rr1[i],xi,RepRules,BranchPointsLetter-> apoles,VAssumptions -> ass]+ModifiedApart[Rr2[i],xi,RepRules,BranchPointsLetter-> apoles,VAssumptions ->ass]; 
]; (*part which deals with Z4s in the numerator*)

Sum[R12[i]Z4[xi,apoles]^(i-1),{i,1,Length[RZCoef]}]
],Row[{ProgressIndicator[Appearance->"Necklace"],"ToEMPLCernelsRational"}," "]]; (*revrite a rational function with Z4[x,a]^n in the numerator as a linear combination of EMPL cernels*)

FindListOfE4s[e_]:=Cases[Variables[e],_E4];(*Find list of all E4 in expression*)(*Corrected in 15.11.19*)
FindListOfZ4s[e_]:=Cases[Variables[e],_Z4];(*Find list of all Z4 in expression*)

(*LinearCoefficientsOfE4s[e_]:=Module[{E4List,rem,coef,i},E4List=FindListOfE4s[e];
coef=Table[D[e,E4List[[i]]]/.{E4List[[i]]->0},{i,1,Length[E4List]}];
rem=Simplify[e-Sum[coef[[i]]E4List[[i]],{i,1,Length[E4List]}]];
{rem,coef,E4List}
];*)(*Old*)

LinearCoefficientsOfE4s[e_]:=Module[{E4List,rem,coef,i,rr},E4List=FindListOfE4s[e];
coef=Table[D[e,E4List[[i]]]/.{E4List[[i]]->0},{i,1,Length[E4List]}];
rr=Thread[E4List->0];
rem=e/.rr;
{rem,coef,E4List}
];

(*Find list coefficients in linear combination of E4s, the output is a list
 {part non linear in E4, coefficients of linear combination of E4(if expression is not linear for some E4 then coefficient=0), list of all E4s in the expression}*)




E4Splitt[a_*b_,RepRules_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,BranchPointsLetter->a,VAssumptions ->{}}]]:=
a*E4Splitt[b,RepRules, Variable -> OptionValue[Variable],EllipticCurve-> OptionValue[EllipticCurve],BranchPointsLetter-> OptionValue[BranchPointsLetter],VAssumptions-> OptionValue[VAssumptions]]/;FreeQ[a,OptionValue[Variable]]&& FreeQ[a,OptionValue[EllipticCurve]];
(*15.11.2019*)

E4Splitt[e_,RepRules_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,BranchPointsLetter->a,VAssumptions ->{}}]]:=
Monitor[Module[{Splitter,i,LCOE4s,E4List,rem,coef,remCernels,coefCernels,eCernels,ePrimitive,intab,yi=OptionValue[EllipticCurve],apoles=OptionValue[BranchPointsLetter],xi=OptionValue[Variable],ass=OptionValue[VAssumptions]},
LCOE4s=LinearCoefficientsOfE4s[e];rem=LCOE4s[[1]];coef=LCOE4s[[2]]; E4List=LCOE4s[[3]];
remCernels= ToEMPLCernelsRational[rem,RepRules,BranchPointsLetter->apoles,Variable->xi,EllipticCurve->yi,VAssumptions ->ass];
coefCernels=Table[ToEMPLCernelsRational[coef[[i]],RepRules,BranchPointsLetter->apoles,Variable->xi,EllipticCurve->yi,VAssumptions ->ass],{i,1,Length[coef]}];
(*eCernels=Expand[remCernels+Sum[coefCernels[[i]]E4List[[i]],{i,1,Length[coef]}]]; *)
eCernels=Expand[remCernels+Sum[coefCernels[[i]]E4List[[i]],{i,1,Length[coef]}],xi]; (***30.01.2020******)

eCernels
],Row[{ProgressIndicator[Appearance->"Necklace"],"E4Splitt"}," "]];
(*Split a linear in E4 function with rational coefficients(contaning Z4 in the numerator) in to the form of eMPL poles wich can be integrated*)  



E4Integrate[e_,{xi_,l1_,l2_},RepRules_List:{},OptionsPattern[{EllipticCurve->y,BranchPointsLetter->a}]]:=
Module[{ePrimitive,yi=OptionValue[EllipticCurve],ai=OptionValue[BranchPointsLetter]},
ePrimitive=E4Primitive[e,xi,RepRules,BranchPointsLetter->ai,EllipticCurve->yi];
((ePrimitive/.{xi->l2,yi->yi[l2],yi[c_]->yi[c]})-(ePrimitive/.{xi->l1,yi->yi[l1],yi[c_]->yi[c]}))/.{yi[xi]->yi}
];(*Integrate a linear combination of E4, yhe coeficients is a rational functions wich can contain Z4 function in the numerator*)



RewriteAllGs[e_,RepRules_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,BranchPointsLetter->a, ReplacmentForSimplify->{},VAssumptions ->{}}]]:=
Monitor[Module[{repForGs,xi=OptionValue[Variable],yi=OptionValue[EllipticCurve],apoles=OptionValue[BranchPointsLetter],srep=OptionValue[ReplacmentForSimplify], ass=OptionValue[VAssumptions]},

glist=FindListOfGs[e];
GLenth=Length[glist];
repForGs=Table[glist[[GProgress]]->  RewriteG2[glist[[GProgress]],RepRules,Variable-> xi,BranchPointsLetter->apoles,EllipticCurve->yi,ReplacmentForSimplify-> srep,VAssumptions ->ass],{GProgress,1,GLenth}];
e/.repForGs
] ,Row[{ProgressIndicator[GProgress/(GLenth)],"  ", GProgress,"/",GLenth, "  Evaluating: ", glist[[GProgress]]},""]];(*Rewrites all MPLs in the expression as EMPLs*)


RewriteG[G[l_,arg_],RepRules_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,BranchPointsLetter->a, ReplacmentForSimplify->{},VAssumptions ->{}}]]:=
Module[{gfp,bv,i,s,de,gf,NLg,gine4s,yInversRep,yRep,glist,repForGs,xi=OptionValue[Variable],yi=OptionValue[EllipticCurve],apoles=OptionValue[BranchPointsLetter],srep=OptionValue[ReplacmentForSimplify],ass=OptionValue[VAssumptions]},

If[FreeQ[l,xi]&&FreeQ[arg,xi]&&FreeQ[l,yi]&&FreeQ[arg,yi],Return[G[l,arg]]];

yInversRep={Sqrt[Product[(xi-apoles[i]),{i,1,4}]]->yi,1/Sqrt[Product[(xi-apoles[i]),{i,1,4}]]->1/yi,Product[xi-apoles[i],{i,1,4}]^n_-> yi^(2*n)};
yRep={yi-> Sqrt[Product[(xi-apoles[i]),{i,1,4}]],1/yi-> Sqrt[Product[(xi-apoles[i]),{i,1,4}]],yi[s_]->yi[s],1/yi[s_]-> 1/yi[s]};  

(*gine4s=SimplifyGCoefficients[DG[G[l,arg]/.yRep,xi]/.yInversRep,arg];*)

gine4s=DG[G[l,arg]/.yRep,xi]/.yInversRep;   (*the best results, tested on S111(1), don't know why*)

(*gine4s=E4SplittGCoefficients[DG[G[l,arg]/.yRep,xi]/.yInversRep,arg,RepRules,Variable-> xi,BranchPointsLetter->apoles,EllipticCurve->yi,VAssumptions ->ass];*)

glist=FindListOfGs[gine4s];

repForGs=Table[glist[[i]]->  RewriteG[glist[[i]],RepRules,Variable-> xi,BranchPointsLetter->apoles,EllipticCurve->yi,ReplacmentForSimplify-> srep,VAssumptions ->ass],{i,1,Length[glist]}];


(*de=SimplifyE4Coefficients[gine4s/.repForGs, srep];*)


de=gine4s/.repForGs;

(*de=RewriteAllGs[gine4s,RepRules,Variable-> xi,BranchPointsLetter->apoles,EllipticCurve->yi,ReplacmentForSimplify-> srep,VAssumptions ->ass];*)

gfp=SimplifyE4Coefficients[E4Primitive[de,xi,RepRules,BranchPointsLetter->apoles,EllipticCurve->yi,VAssumptions ->ass]/.{yi[xi]->yi},srep];

gf=gfp-(gfp/.{xi->0,yi->yi[0],yi[c_]->yi[c],1/yi-> 1/yi[0],1/yi[c_]-> 1/yi[c]});

If[ass=={},bv=BoundaryValueGforE4[G[l,arg],RepRules,BranchPointsLetter->apoles,EllipticCurve->yi,Variable->xi],bv=(Assuming[##,BoundaryValueGforE4[G[l,arg],RepRules,BranchPointsLetter->apoles,EllipticCurve->yi,Variable->xi]]&@@ ass)];
NLg= gf + bv ; 
NLg
]; (*Rewrites one given MPL as EMPL*)




RewriteG2[G[l_,arg_],RepRules_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,BranchPointsLetter->a, ReplacmentForSimplify->{},VAssumptions ->{}}]]:=
Module[{KastylEbat228,Simpl,listForInt,cofglistExpanded,cofglist,gfp,bv,i,s,de,dg,gf,NLg,gine4s,yInversRep,yRep,glist,repForGs,xi=OptionValue[Variable],yi=OptionValue[EllipticCurve],apoles=OptionValue[BranchPointsLetter],srep=OptionValue[ReplacmentForSimplify],ass=OptionValue[VAssumptions]},

(*******************SimplCode*******************************************)
     Simpl[a_+b_,x_]:=Simpl[a,x]+Simpl[b,x];
Simpl[a_./(x_+b_.),x_]:=If[ass=={},FullSimplify[a],Assuming[##,FullSimplify[a]]&@@ ass]/(x+b) ;
Simpl[a_.,x_]:=If[ass=={},FullSimplify[a],Assuming[##,FullSimplify[a]]&@@ ass]/;FreeQ[a,x]; 
Simpl[x_*a_.,x_]:=x*If[ass=={},FullSimplify[a],Assuming[##,FullSimplify[a]]&@@ ass]/;FreeQ[a,x];

(*****************************************************************************)

If[FreeQ[l,xi]&&FreeQ[arg,xi]&&FreeQ[l,yi]&&FreeQ[arg,yi],Return[G[l,arg]]];
If[Length[l]==1,Return[RewriteG[G[l,arg],RepRules,Variable-> xi,BranchPointsLetter->apoles,EllipticCurve->yi,ReplacmentForSimplify-> srep,VAssumptions ->ass]]];

yInversRep={Sqrt[Product[(xi-apoles[i]),{i,1,4}]]->yi,1/Sqrt[Product[(xi-apoles[i]),{i,1,4}]]->1/yi,Product[xi-apoles[i],{i,1,4}]^n_-> yi^(2*n)};
yRep={yi-> Sqrt[Product[(xi-apoles[i]),{i,1,4}]],1/yi-> Sqrt[Product[(xi-apoles[i]),{i,1,4}]],yi[s_]->yi[s],1/yi[s_]-> 1/yi[s]};  

(*gine4s=SimplifyGCoefficients[DG[G[l,arg]/.yRep,xi]/.yInversRep,arg];*)

dg=SimplifyArgOfTF[DG[G[l,arg]/.yRep,xi]/.yInversRep]; 

(*gine4s=E4SplittGCoefficients[DG[G[l,arg]/.yRep,xi]/.yInversRep,arg,RepRules,Variable-> xi,BranchPointsLetter->apoles,EllipticCurve->yi,VAssumptions ->ass];*)

glist=FindListOfGs[dg];

cofglist=Table[Coefficient[dg,glist[[i]]],{i,1,Length[glist]}];


repForGs=Table[DeleteCases[List@@(RewriteG2[glist[[i]],RepRules,Variable-> xi,BranchPointsLetter->apoles,EllipticCurve->yi,ReplacmentForSimplify-> srep,VAssumptions ->ass]+KastylEbat228),KastylEbat228],{i,1,Length[glist]}];

cofglistExpanded=Table[DeleteCases[List@@(Simpl[Expand[E4Splitt[cofglist[[i]],RepRules,Variable-> xi,BranchPointsLetter->apoles,EllipticCurve->yi,VAssumptions ->ass],xi],xi]+KastylEbat228),KastylEbat228],{i,1,Length[glist]}]/.{Simpl[Sarg_,Sx_]:> Sarg};

listForInt=Flatten[Table[Flatten[Table[cofglistExpanded[[i]][[k]]*repForGs[[i]][[j]],{j,1,Length[repForGs[[i]]]},{k,1,Length[cofglistExpanded[[i]]]}]],{i,1,Length[glist]}]];


gfp=SimplifyE4Coefficients[Plus @@Table[E4Primitive[listForInt[[i]],xi,RepRules,EllipticCurve->yi,BranchPointsLetter->apoles,VAssumptions ->ass],{i,1,Length[listForInt]}],srep];

gf=gfp-(gfp/.{xi->0,yi->yi[0],yi[c_]->yi[c],1/yi-> 1/yi[0],1/yi[c_]-> 1/yi[c]});

If[ass=={},bv=BoundaryValueGforE4[G[l,arg],RepRules,BranchPointsLetter->apoles,EllipticCurve->yi,Variable->xi],bv=(Assuming[##,BoundaryValueGforE4[G[l,arg],RepRules,BranchPointsLetter->apoles,EllipticCurve->yi,Variable->xi]]&@@ ass)];
NLg= gf + bv; 
NLg
]; (*Rewrites one given MPL as EMPL*)


BoundaryValueGforE4[G[l_,arg_],RepRules_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,BranchPointsLetter->a}]]:=
Monitor[Module[{j,lNew,argNew,xi=OptionValue[Variable],yi=OptionValue[EllipticCurve],apoles=OptionValue[BranchPointsLetter]},
lNew=Normal[Series[(l/.{yi->  Sqrt[Product[(xi-apoles[j]),{j,1,4}]],yi[b_] :>  yi[b]})/.RepRules,{xi,0,2}]];
argNew=Normal[Series[(arg/.{yi->  Sqrt[Product[(xi-apoles[j]),{j,1,4}]],yi[b_] :>  yi[b]})/.RepRules,{xi,0,2}]];
FullSimplify[BoundaryValueGforG[G[lNew,argNew],xi]]
],Row[{ProgressIndicator[Appearance->"Necklace"],"BoundaryValueGforE4"}," "]];(*15.11.2019*)


End[]
