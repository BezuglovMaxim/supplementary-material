(* ::Package:: *)

Begin["Private`"]


SplittSumConditionaly[e_,y_]:=Module[{a={},l,ex1,ex2},l=List @@ Expand[e];
For[i=1,i<=Length[l],i++,
If[FreeQ[l[[i]],y],a=Insert[a,l[[i]],-1]]
];
ex1=Simplify[Plus  @@a];
ex2=Simplify[Expand[e]-ex1];
{ex1,ex2}
]


End[]
