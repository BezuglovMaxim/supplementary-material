(* ::Package:: *)

RemoveFromBack


RemoveFromFront


Begin["Private`"]


ShuffleAlgebra[ta_List,tb_List]:=
Module[{Shuffle,ModifiedInsert},
ModifiedInsert[t_List,x_]:=Insert[t,x,Table[{i,-1},{i,1,Length[t]}]];(*Insert element x in the end of every element of Matrix t({{},{},...})*)
Shuffle[t1_List,t2_List]:=Shuffle[t1,t2]=Piecewise[{{Join[ModifiedInsert[Shuffle[Delete[t1,-1],t2],t1[[-1]]],ModifiedInsert[Shuffle[t1,Delete[t2,-1]],t2[[-1]]]],Length[t1]>=1 && Length[t2]>=1},{{t2},Length[t1]==0},{{t1},Length[t2]==0}}];
Shuffle[ta,tb]
];(*Gives a shuffle product of two lists*)

ShuffleSimplify[e_]:=
Block[{ShS, $IterationLimit=400000,$RecursionLimit=400000},
ShS[a_]:=ShS[Expand[a]];
ShS[a_+b_]:=  ShS[a]+ShS[b]; 
ShS[a_]:=a/;(FreeQ[a,E4]&&FreeQ[a,G]&&FreeQ[a,X]);
ShS[a_*b_]:=a*ShS[b]/;(FreeQ[a,E4]&& FreeQ[a,G]&& FreeQ[a,X]);

ShS[E4[a1_List,arg_,br_]]:=E4[a1,arg,br];
ShS[G[a1_List,arg_]]:=G[a1,arg];
ShS[X[a1_List]]:=X[a1];
ShS[E4[a1_List,arg1_,br_]G[a2_List,arg2_]]:=E4[a1,arg1,br]G[a2,arg2];

ShS[E4[a1_List,arg_,br_]*E4[b1_List,arg_,br_]*a_.]:=ShS[Sum[E4[ShuffleAlgebra[a1,b1][[i]],arg,br],{i,1,Length[ShuffleAlgebra[a1,b1]]}]*a];
ShS[G[a1_List,arg_]*G[b1_List,arg_]*a_.]:=ShS[Sum[G[ShuffleAlgebra[a1,b1][[i]],arg],{i,1,Length[ShuffleAlgebra[a1,b1]]}]*a];
ShS[X[a1_List]*X[b1_List]*a_.]:=ShS[Sum[X[ShuffleAlgebra[a1,b1][[i]]],{i,1,Length[ShuffleAlgebra[a1,b1]]}]*a];

ShS[G[a1_List,arg1_]*G[b1_List,arg2_]]:=G[a1,arg1]*G[b1,arg2]/;(ToString[arg1]!=ToString[arg2]);
ShS[G[a1_List,arg1_]*G[b1_List,arg2_]*G[c1_List,arg3_]]:=G[a1,arg1]*G[b1,arg2]*G[c1,arg3]/;(ToString[arg1]!=ToString[arg2])&&(ToString[arg1]!=ToString[arg3]);

ShS[E4[a1_List,arg_,br_]^n_ a_.]:=ShS[Sum[E4[ShuffleAlgebra[a1,a1][[i]],arg,br],{i,1,Length[ShuffleAlgebra[a1,a1]]}]*E4[a1,arg,br]^(n-2)*a]/;n>= 2;
ShS[G[a1_List,arg_]^n_ a_.]:=ShS[Sum[G[ShuffleAlgebra[a1,a1][[i]],arg],{i,1,Length[ShuffleAlgebra[a1,a1]]}]*G[a1,arg]^(n-2)*a]/;n>= 2;
ShS[X[a1_List]^n_ a_.]:=ShS[Sum[X[ShuffleAlgebra[a1,a1][[i]]],{i,1,Length[ShuffleAlgebra[a1,a1]]}]*X[a1]^(n-2)*a]/;n>= 2;

ShS[e]
];(*Simplifies the expresion with E4 and G using shuffle algebra, i.e. rvrites all permutations of E4 and G as linear combinations*)


RemoveFromBack[ta_List,tr_List]:=
Module[{RFB},
RFB[ta1_List, tr1_List]:=RFB[ta1, tr1]=Piecewise[{{X[ta1],ContainsNone[{ta1[[-1]]},tr1]},{X[ta1],ContainsOnly[ta1,tr1]},{X[Delete[ta1,-1]]X[{ta1[[-1]]}]-
Simplify[ShuffleSimplify[X[Delete[ta1,-1]]X[{ta1[[-1]]}]]-X[ta1]],ContainsOnly[{ta1[[-1]]},tr1]&&ContainsNone[{ta1[[-2]]},tr1]  (*One particular case, it can be fully coverd by default walue*) }},
Module[{k,l1,l2},
(*Splitting ta1 in to two parts, one part contains all last elemets with tr elements and the second part ends with an element not from the tr list*)
k=1;
While[ContainsAny[{ta1[[-k]]},tr1],k++];
l1=Drop[ta1,Length[ta1]-k+1];(*First part*)
l2=Drop[ta1,-k+1]; (*Second part*)
(***************************************************)
X[l1]X[l2]-(Simplify[ShuffleSimplify[X[l1]X[l2]]-X[ta1]]/.{X[ta2_] :>   RFB[ta2,tr1]})  
]];
RFB[ta,tr]
];(*Removes a given combination of letters from the end of the list ta_List using the shuffle algebra*)

RemoveFromFront[ta_List,tr_List]:=
Module[{RFF},
RFF[ta1_List, tr1_List]:=RFF[ta1, tr1]=Piecewise[{{X[ta1],ContainsNone[{ta1[[1]]},tr1]},{X[ta1],ContainsOnly[ta1,tr1]},{X[Delete[ta1,1]]X[{ta1[[1]]}]-
Simplify[ShuffleSimplify[X[Delete[ta1,1]]X[{ta1[[1]]}]]-X[ta1]],ContainsOnly[{ta1[[1]]},tr1]&&ContainsNone[{ta1[[2]]},tr1]  (*One particular case, it can be fully coverd by default walue*) }},
Module[{k,l1,l2},
(*Splitting ta1 in to two parts, one part contains all last elemets with tr elements and the second part ends with an element not from the tr list*)
k=1;
While[ContainsAny[{ta1[[k]]},tr1],k++];
l1=ta1[[1 ;; k-1]];(*First part*)
l2=ta1[[k ;; Length[ta1]]]; (*Second part*)
(***************************************************)
X[l1]X[l2]-(Simplify[ShuffleSimplify[X[l1]X[l2]]-X[ta1]]/.{X[ta2_] ->    RFF[ta2,tr1]}) 
]];
RFF[ta,tr]
];(*Removes a given combination of letters from the front of the list ta_List using the shuffle algebra*)

(********************Regularization*****************************)

(*RegularizeG[e_, OptionsPattern[{RemoveZeros -> True}]]:=Module[{RegG},
RegG[a_]:=RegG[Expand[a]];
RegG[a_+b_]:= RegG[a]+RegG[b]; 
RegG[a_*b_]:= RegG[a]*RegG[b];
RegG[a_]:=a/; FreeQ[a,G];
RegG[a_*b_]:=a*RegG[b]/; FreeQ[a,G];
RegG[f_[a___]]:=RegG/@f[a];

RegG[G[a1_List,arg_]]:=(RemoveFromFront[a1,{arg}]/.{X[l_]-> G[l,arg]})/; !OptionValue[RemoveZeros];
RegG[G[a1_List,arg_]]:=(RemoveFromFront[a1,{arg}]/.{X[l_]->  RemoveFromBack[l,{0}]}/.{X[l_]-> G[l,arg]})/; OptionValue[RemoveZeros];

RegG[e]
];*)
RegularizeG[e_, OptionsPattern[{RemoveZeros -> True}]]:=Module[{RegG,l,rep,i},
l=FindListOfGs[e];
RegG[G[a1_List,arg_]]:=(RemoveFromFront[a1,{arg}]/.{X[l_]-> G[l,arg]})/; !OptionValue[RemoveZeros];
RegG[G[a1_List,arg_]]:=(RemoveFromFront[a1,{arg}]/.{X[l_]->  RemoveFromBack[l,{0}]}/.{X[l_]-> G[l,arg]})/; OptionValue[RemoveZeros];
rep=Table[l[[i]]-> RegG[l[[i]]],{i,1,Length[l]}];
e/.rep
];

RegularizeGInAPoint[G[l_,f_],lim_]:=Module[{aa,ll,a,n=Length[l],zeros={},sing={},i},ll=Simplify[Limit[l/f,lim]];aa=Table[a[i],{i,1,n}];
For[i=1,i<=n,i++,
If[ToString[ll[[i]]]=="1",sing=Insert[sing,a[i],-1]];
If[ToString[ll[[i]]]=="0",zeros=Insert[zeros,a[i],-1]];
];
(RemoveFromFront[aa,sing]/.{X[arg_]-> RemoveFromBack[arg,zeros]}/.{X[arg_]-> G[arg,f]})/.Thread[aa-> l]
];

RegularizeGInAPoint[e_,lim_]:=Module[{l,rep},
l=FindListOfGs[e];
rep=Table[l[[i]]-> RegularizeGInAPoint[l[[i]],lim],{i,1,Length[l]}];
e/.rep
];



RegularizeE4[e_, OptionsPattern[{RemoveZeros -> True}]]:=Module[{RegE4},
RegE4[a_]:=RegE4[Expand[a]];
RegE4[a_+b_]:= RegE4[a]+RegE4[b]; 
RegE4[a_*b_]:= RegE4[a]*RegE4[b];
RegE4[a_]:=a/; FreeQ[a,E4];
RegE4[a_*b_]:=a*RegE4[b]/; FreeQ[a,E4];
RegE4[f_[a___]]:=RegE4/@f[a];

(*RegE4[E4[a1_List,arg_,br_]]:=RemoveFromBack[a1,{{1,0},{-1,0}}]/.{X[l_]\[RuleDelayed] E4Reg[E4[l,arg,br]]};*)

RegE4[E4[a1_List,arg_,br_]]:=(RemoveFromFront[a1,{{1,arg},{-1,arg}}]/.{X[l_]-> E4[l,arg,br]})/; !OptionValue[RemoveZeros];
RegE4[E4[a1_List,arg_,br_]]:=(RemoveFromFront[a1,{{1,arg},{-1,arg}}]/.{X[l_]->  RemoveFromBack[l,{{1,0},{-1,0}}]}/.{X[l_]-> E4[l,arg,br]})/; OptionValue[RemoveZeros];

RegE4[e]
];


(*E4Reg[e_]:=e/.{E4[{{-1,0}},x_,a_]-> Log[x]+ER4[{},{-1,0},x,a], E4[{{1,0}},x_,a_]-> Log[x],
E4[{{1,0},{-1,0}},x_,a_]-> 1/2 (Log[x])^2+ER4[{{1,0}},{-1,0},x,a],
E4[{{-1,0},{1,0}},x_,a_]-> 1/2 (Log[x])^2+Log[x]ER4[{},{-1,0},x,a]-ER4[{{1,0}},{-1,0},x,a],
E4[{{-1,0},{-1,0}},x_,a_]-> 1/2 (Log[x])^2+Log[x]ER4[{},{-1,0},x,a]-ER4[{{-1,0}},{-1,0},x,a]+ER4[{{1,0}},{-1,0},x,a], 
E4[{{1,0},{1,0}},x_,a_]-> 1/2 (Log[x])^2}(*a list of reguralized E4s for special cases, needs to be rewritten for general case*)*)

GReg[e_]:=e/.{G[l_,a_] /; ContainsOnly[l,{0}] :>  1/Length[l]! Log[a]^Length[l]};(*a list of reguralized Gs *)
(*ShuffleReguralize[e_]:=E4Reg[e/.{E4[l_,x_,a_]->E4RemoveFromBack[l,x, a,{{1,0},{-1,0}}]}]; (*Regularize the  expression contaning E4 functions*)*)

E4Reg[a_+b_]:=E4Reg[a]+E4Reg[b];
E4Reg[a_]:=a/;FreeQ[a,E4];
E4Reg[a_*b_]:=a*E4Reg[b]/;FreeQ[a,E4];
E4Reg[a_*b_]:=E4Reg[a]*E4Reg[b];
E4Reg[E4[l_,x_,a_]]:=Module[{output},
If[ContainsOnly[l,{{1,0}}],Return[Log[x]^Length[l]/Length[l]!]];
If[l[[-1]]=={-1,0},output=ER4[Delete[l,-1],l[[-1]],x,a]+E4[Insert[Delete[l,-1],{1,0},-1],x,a]];
If[l[[-1]]=={1,0},output=RemoveFromBack[l,{{1,0}}]/.{X[li_]:>E4[li,x,a]}];
output=E4Reg[output];
Expand[output]
];


(***************************************************************************************************************************************************************************)
(******************************************************************************)
E4Visualize[e_]:=e/.{E4[l_List,x_,al_]-> E4[(Transpose[l]//MatrixForm),x,al]};(*Visualization of E4 function*)




End[]
