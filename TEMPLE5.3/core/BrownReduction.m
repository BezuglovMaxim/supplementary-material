(* ::Package:: *)

Begin["Private`"]


(* [1] Brown,F.(2009).The massless higher-loop two-point function.Communications in Mathematical Physics,287(3),925-958. *)
(***********************************************************************************************************)
 FindIrreducible[f_List]:=
Module[{t,tt},t=Table[Delete[FactorList[f[[q1]]],1],{q1,1,Length[f]}];
tt=Flatten[t,1];
Table[Flatten[t,1][[q1]][[1]],{q1,1,Length[tt]}]
]; (*Find all irreducible factors of ol polinomials from the list f and form a new list owt of those factors. Step (2) of the simple reduction algoritm in [1](p. 13)  *)
(*****)
(*Remove_Constants_and_Monomials*)
RCaM[f_List]:=
Module[{f1},f1={};
For[i=1,i<=Length[f],i++,
If[Length[CoefficientRules[ f[[i]] ]]>1 ||Length[ Variables[f[[i]]]]>1,f1=Insert[f1, f[[i]],-1]];
];
f1];(*Remove all constants and monomials oof form x[i] from the list f. Remark 9 for the simple reduction algoritm in [1](p. 13)*)
(*****)
LinearCheck[f_List,x_]:=
Module[{i,t},
i=1;t=True ;While[i<=Length[f] && t, If[Length[CoefficientList[f[[i]],x]]>= 3,t=False];i++];t
]; (*Checks wether every element in list f is linear in variable x. Returns True if every element in list f is linear in variable x and False otherwise *)
(**********)
SimpleReduction[S_List,n_Integer,x_]:=
Module[{L,g,h,f},L=Length[S];
If[LinearCheck[S,x[n]],g=Table[D[S[[q1]],x[n]],{q1,1,L}];
h=Table[S[[q1]]/.{x[n]-> 0},{q1,1,L}];
f=RCaM[FindIrreducible[Flatten[Table[Table[(h[[i]]g[[j]]-g[[i]]h[[j]]),{i,1,j-1}],{j,2,L}]]]];
g=RCaM[FindIrreducible[g]];
h=RCaM[FindIrreducible[h]];
Union[Join[g,h,f]],{Undefined}]
];(*The simple reduction algoritm from [1](p. 13), S - is input list of polinomials in variables x[i_] and n - is reduction index. If every polinomial in the list
 is linear in variable x[i] then will return the reducted list of polinomials, otherwise will return {Undefined}*)
(*******)
BrownReduction[functions_List,reductionindexes_List,OptionsPattern[{Variable->x}]]:=
Module[{FubiniReduction1,xi=OptionValue[Variable]},
FubiniReduction1[f_List,ri_List,xxx_]:=FubiniReduction1[f,ri,xxx]=Piecewise[{{SimpleReduction[f,ri[[1]],xi],Length[ri]==1},
{Intersection[SimpleReduction[SimpleReduction[f,ri[[1]],xxx],ri[[2]],xxx],SimpleReduction[SimpleReduction[f,ri[[2]],xxx],ri[[1]],xxx]],Length[ri]==2},
{Apply[Intersection,DeleteCases[Table[SimpleReduction[FubiniReduction1[f,Delete[ri,q1],xxx],ri[[q1]],xxx],{q1,1,Length[ri]}],{Undefined}]],Length[ri]>2}}];
FubiniReduction1[functions,reductionindexes,xi]
];(*The Brown Reduction algorithm*)
(***********************************************************************************************************)


End[]
