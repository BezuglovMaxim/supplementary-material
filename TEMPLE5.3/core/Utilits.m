(* ::Package:: *)

FullSimplifyArgOfTF


Begin["Private`"]


SplittSumConditionaly[e_,y_]:=Module[{a={},l,ex1,ex2},l=List @@ Expand[e];
For[i=1,i<=Length[l],i++,
If[FreeQ[l[[i]],y],a=Insert[a,l[[i]],-1]]
];
ex1=Simplify[Plus  @@a];
ex2=Simplify[Expand[e]-ex1];
{ex1,ex2}
];

BezuTheorem[polinom_,v_,coef_,l_List]:=Module[{s,p,g,h,k=Length[l],i},
If[Exponent[polinom,v]!= k,Print["Power of polinom is different from number of roots in list"];Return[Null]];
s=Solve[polinom==0,v];
p={polinom-> coef*Product[v-l[[i]],{i,1,k}]};
g={coef-> Coefficient[polinom,v^k]};
h=Table[l[[i]]-> (v/.s[[i]]),{i,1,k}];
{p,Join[g,h]}
]



FullSimplifyE4Coefficients[e_,RepRules1_List:{}]:=Monitor[Module[{E4list,E4RowCoefficients,E4CoefficientsSimplified},
E4list=FindListOfE4s[e];
lenth=Length[E4list];
E4RowCoefficients=Table[Coefficient[e,E4list[[i]]],{i,1,lenth}];
E4CoefficientsSimplified=Table[0,{i,1,lenth}];
 For[progress=1,progress<=lenth,progress++,
E4CoefficientsSimplified[[progress]]=FullSimplify[E4RowCoefficients[[progress]]/.RepRules1];
];

Sum[E4CoefficientsSimplified[[i]]*E4list[[i]],{i,1,lenth}]+FullSimplify[e/.{E4[l_,x_,a_]->0}]
],Row[{ProgressIndicator[progress/(lenth+1)],(progress*1.0)/(lenth*1.0+1.0)*100 "%"}," "]];


SimplifyE4Coefficients[e_,RepRules1_List:{}]:=Monitor[Module[{E4list,E4RowCoefficients,E4CoefficientsSimplified},
progress=0;
E4list=FindListOfE4s[e];
lenth=Length[E4list];
E4RowCoefficients=Table[Coefficient[e,E4list[[i]]],{i,1,lenth}];
E4CoefficientsSimplified=Table[0,{i,1,lenth}];
 For[progress=1,progress<=lenth,progress++,
E4CoefficientsSimplified[[progress]]=Simplify[E4RowCoefficients[[progress]]/.RepRules1];
];

Sum[E4CoefficientsSimplified[[i]]*E4list[[i]],{i,1,lenth}]+Simplify[(e/.{E4[l_,x_,a_]->0})/.RepRules1]
],Row[{ProgressIndicator[progress/(lenth+1)],(progress*1.0)/(lenth*1.0+1.0)*100 "%"}," "]];


SimplifyGCoefficients[e_,v_,RepRules1_List:{}]:=Monitor[Module[{Glist,E4RowCoefficients,E4CoefficientsSimplified,i},
Glist=FindListOfGs[e];

For[i=1,i<= Length[Glist],i++,
If[FreeQ[Glist[[i]],v],Glist=Delete[Glist,i];i=i-1];
];

(*Glist*)
lenth=Length[Glist];
E4RowCoefficients=Table[Coefficient[e,Glist[[i]]],{i,1,lenth}];
E4CoefficientsSimplified=Table[0,{i,1,lenth}];
 For[progress=1,progress<= lenth,progress++,
E4CoefficientsSimplified[[progress]]=Simplify[E4RowCoefficients[[progress]]/.RepRules1,TimeConstraint->2000];
];

Sum[E4CoefficientsSimplified[[i]]*Glist[[i]],{i,1,lenth}]+Simplify[e/.{G[l_,v]->0},TimeConstraint->2000]
],Row[{ProgressIndicator[progress/lenth],(progress*1.0)/(lenth*1.0)*100 "%"}," "]];


E4SplittGCoefficients[e_,v_,arep_List:{},OptionsPattern[{Variable->x,EllipticCurve->y,PolesLetter->a,VAssumptions ->{}}]]:=
Monitor[Module[{GlistRaw,Glist,GRowCoefficients,GCoefficientsSimplified,i,yi=OptionValue[EllipticCurve],apoles=OptionValue[PolesLetter],xi=OptionValue[Variable],ass=OptionValue[VAssumptions]},

GlistRaw=FindListOfGs[e];

Glist=Cases[GlistRaw,G[{l___},v]->   G[{l},v]]; 
lenth=Length[Glist];

GRowCoefficients=Table[Coefficient[e,Glist[[i]]],{i,1,lenth}];

GCoefficientsSimplified=Table[0,{i,1,lenth}];

 For[progress=1,progress<= lenth,progress++,
GCoefficientsSimplified[[progress]]=E4Splitt[GRowCoefficients[[progress]],arep,EllipticCurve->yi,PolesLetter->apoles,Variable->xi,VAssumptions -> ass];
];

Sum[GCoefficientsSimplified[[i]]*Glist[[i]],{i,1,lenth}]+Simplify[e/.{G[l_,v]->0}]

],Row[{ProgressIndicator[progress/(lenth+1)],(progress*1.0)/(lenth*1.0+1)*100 "%"}," "]];


SimplifyArgOfTF[e_]:=(e/.{G[a___]:> Simplify/@G[a],E4[b___]:> Simplify/@E4[b]}); 


FullSimplifyArgOfTF[e_]:=(e/.{G[a___]:> FullSimplify/@G[a],E4[b___]:> FullSimplify/@E4[b]}); 


End[]
