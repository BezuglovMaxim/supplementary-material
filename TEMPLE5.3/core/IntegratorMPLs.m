(* ::Package:: *)

GasGListOfElements={};
GasGListOfReplacments={};


Begin["Private`"]


G[{},x_]:=1;
G[l_,0]:=0 /;!ContainsOnly[l,{0}];
(*********************************************************************)
GPrimitive[expr_,variable_]:=Monitor[Block[{$RecursionLimit=2000,Gprim},
(****************Basics*******************) 
Gprim[f1_ + f2_, x_] := Gprim[f1,x] +Gprim[f2, x];
       Gprim[c_ f_, x_] := c Gprim[f, x] /; FreeQ[c, x] ;
Gprim[a_./(b_.*c_+d_.*c_),x_]:=1/(b+d) Gprim[a/c, x] /; FreeQ[{b,d}, x]; 
(****************MPLs********************)
Gprim[1/(x_+a_.),x_]:= G[{-a},x]/; FreeQ[{a}, x]; 
Gprim[1/(x_+a_.) G[l_,x_],x_]:= G[Insert[l,-a,1],x]/; FreeQ[{a,l}, x];
(****************Integration by parts***********************************)

Gprim[G[l_,x_],x_]:=x*G[l,x]- Gprim[x/(x-l[[1]])* G[Delete[l,1],x],x]/; FreeQ[l, x];
        Gprim[x_^n_.*G[l_,x_],x_]:=(x^(n+1)*G[l,x])/(n+1 ) -1/(n+1) *Gprim[x^(n+1)*1/(x-l[[1]])* G[Delete[l,1],x],x]/;n!= -1&&  FreeQ[l, x]; 
Gprim[(x_+a_.)^n_.*G[l_,x_],x_]:=((x+a)^(n+1)*G[l,x])/(n+1 ) -1/(n+1) *Gprim[(x+a)^(n+1)*1/(x-l[[1]])* G[Delete[l,1],x],x]/;n!= -1 &&  FreeQ[{a,l}, x];
(****************Simple rules*************)
 Gprim[c_, t_] := c t /; FreeQ[c, t];
        Gprim[t_^n_., t_] := 1/(n + 1) t^(n + 1) /; n != -1;
        Gprim[(u_ + t_)^n_., t_] := 1/(n + 1) ((u + t)^(n + 1) ) /; (n != -1) && FreeQ[u, t];
        Gprim[(u_ + v_*t_)^n_., t_] := 1/(n + 1)/v ((u + v*t)^(n + 1)) /; (n != -1) && FreeQ[{u, v}, t];
        Gprim[(u_.*t_ + v_ + w_. t_)^n_., t_] := Gprim[((u + w)*t + v)^n,t] /; (n != -1) && FreeQ[{u, v, w}, t];
 Gprim[Exp[c_ *t_],t_]:=Exp[c *t]/c/; FreeQ[c, t];
 (*G splitt**)
 Gprim[a_,x_]:=Gprim[GSplitt[a,x],x];
 
Gprim[expr,variable]
],Row[{ProgressIndicator[Appearance->"Necklace"],"GPrimitive"}," "]];


FindListOfGs[e_]:=Cases[Variables[e],_G];



ZeroList[polinomial_,x_:x]:=Module[{i,s},s=Solve[polinomial==0,x];Union[Table[x/.s[[i]],{i,1,Length[s]}]]];

ModifiedApartG[rational_,x_:x]:=Monitor[Module[{zl,d,rem,poles,i}, d=Denominator[rational]; 
zl=ZeroList[d,x]; poles=Sum[Normal[Series[rational,{x,zl[[i]],-1}]],{i,1,Length[zl]}]+Normal[Series[rational,{x,Infinity,0}]];
poles+FullSimplify[rational-poles]
],Row[{ProgressIndicator[Appearance->"Necklace"],"ModifiedApartG"}," "]]; 

LinearCoefficientsOfGs[e_]:=Module[{GList,rem,coef,i},GList=FindListOfGs[e];
coef=Table[D[e,GList[[i]]]/.{GList[[i]]->0},{i,1,Length[GList]}];
rem=Simplify[e-Sum[coef[[i]]GList[[i]],{i,1,Length[GList]}]];
{rem,coef,GList}
]; 

GSplitt[e_,x_:x]:=
Monitor[Module[{i,LCOGs,GList,rem,coef,remCernels,coefCernels,eCernels},
LCOGs=LinearCoefficientsOfGs[e];rem=LCOGs[[1]];coef=LCOGs[[2]]; GList=LCOGs[[3]];

remCernels= ModifiedApartG[Together[rem],x];(*Together edited 3.10.2019*)
coefCernels=Table[ModifiedApartG[Together[coef[[i]]],x],{i,1,Length[coef]}];(*Together edited 3.10.2019*)

eCernels=Expand[remCernels+Sum[coefCernels[[i]]GList[[i]],{i,1,Length[coef]}]];

eCernels
],Row[{ProgressIndicator[Appearance->"Necklace"],"GSplitt"}," "]];    

GIntegrate[e_,limits_List]:=
Module[{gPrimitive},
gPrimitive=GPrimitive[e,limits[[1]]];
(gPrimitive/.{limits[[1]]->limits[[3]]})-(gPrimitive/.{limits[[1]]->limits[[2]]})
];

PartialDG[G[l_,f_],n_]:=Module[{i,right=Drop[l,n],left=Drop[l,-(Length[l]-n+1)], middle=l[[n]],p,XInnerVariable},
p=GIntegrate[1/(XInnerVariable[n]- middle)^2 G[right,XInnerVariable[n]],{XInnerVariable[n],0,XInnerVariable[n-1]}];

For[i=1,i< n,i++,
p=GIntegrate[1/(XInnerVariable[n-i]-left[[-i]]) p,{XInnerVariable[n-i],0,XInnerVariable[n-i-1]}];
];
XInnerVariable[0]=f;
p
];

DGSingle[G[l_,f_],x_]:= G[l,f]/;FreeQ[l,x]&&FreeQ[f,x];

DGSingle[G[l_,f_],x_]:=Module[{i,n=Length[l]},
D[f,x]/(f-l[[1]]) G[Delete[l,1],f]+Sum[If[FreeQ[l[[i]],x],0,D[l[[i]],x]*PartialDG[G[l,f],i]],{i,1,n}]
]; 

DG[expr_,xx_]:=Block[{Psi,i,dG},
(******************General rules******************************************************************)
dG[e_,x_]:=D[e,x]/;FreeQ[e,G]; 
dG[a_+b_,x_]:=dG[a,x]+dG[b,x];
dG[a_*b_,x_]:=a*dG[b,x]/;FreeQ[a,x];
dG[a_*b_,x_]:=dG[a,x]b+dG[b,x]a;
dG[a_/b_,x_]:=(dG[a,x]b-dG[b,x]a)/b^2 ;
(*dE4[f_[g_],x_]:=D[f[g],g]dE4[g,x];*)
(*******************MPL**************************************************************************)
dG[G[l_,f_],x_]:=DGSingle[G[l,f],x];
(*************************************************************************************************)
dG[RegularizeG[expr],xx]
];

(*RewriteAllGsAsG[e_,OptionsPattern[{Variable->x}]]:=Module[{GinE4,xi=OptionValue[Variable]},
GinE4[a_+b_]:=GinE4[a]+GinE4[b];
GinE4[v_*c_]:=GinE4[v]GinE4[c]; 
GinE4[a_]:=a/;FreeQ[a, G];
GinE4[G_[a___]]:=RewriteGasG[G[a],Variable->xi];  
GinE4[e]
]; (*Rewrites all MPLs in the expression of other MPLs*)*)

RewriteAllGsAsG[e_,OptionsPattern[{Variable->x}]]:=
Monitor[Module[{repForGs,xi=OptionValue[Variable]},
glistG=FindListOfGs[e];
GLenthG=Length[glistG];
repForGs=Table[glistG[[GProgress]]-> RewriteGasG2[glistG[[GProgress]],Variable-> xi],{GProgress,1,GLenthG}];
e/.repForGs
] ,Row[{ProgressIndicator[GProgress/(GLenthG)],"  ", GProgress,"/",GLenthG, "  Evaluating: ", glistG[[GProgress]]},""]];(*Rewrites all MPLs in the expression as EMPLs*)





RewriteGasG[G[l_,arg_],OptionsPattern[{Variable->x}]]:=
Module[{i,s,de,gf,NLg,gine4s,glist,repForGs,xi=OptionValue[Variable],final},

If[FreeQ[l,xi]&&FreeQ[arg,xi],Return[G[l,arg]]];
If[ContainsAny[GasGListOfElements,{G[l,arg]}],Return[G[l,arg]/.GasGListOfReplacments]];

gine4s=SimplifyGCoefficients[DG[G[l,arg],xi],arg];

glist=FindListOfGs[gine4s];

repForGs=Table[glist[[i]]-> RewriteGasG[glist[[i]],Variable-> xi],{i,1,Length[glist]}];

de=SimplifyGCoefficients[gine4s/.repForGs,xi];

gf=GIntegrate[de,{xi,0,xi}];
NLg=gf + BoundaryValueGforG[G[l,arg],xi]; 
final=SimplifyGCoefficients[NLg,xi];
If[ContainsNone[GasGListOfElements,{G[l,arg]}], GasGListOfElements=Insert[GasGListOfElements,G[l,arg],-1];GasGListOfReplacments=Insert[GasGListOfReplacments,G[l,arg]->final ,-1]];
final
]; (*Rewrites one given MPL as MPL*)(*03.12.2019*)

RewriteGasG2[G[l_,arg_],OptionsPattern[{Variable->x}]]:=
Module[{remeaningPart,KastylEbat228,Simpl,listForInt,cofglistExpanded,cofglist,gfp,bv,i,s,de,dg,gf,NLg,gine4s,yInversRep,yRep,glist,repForGs,xi=OptionValue[Variable]},

(*******************SimplCode*******************************************)
Simpl[a_+b_,x_]:=Simpl[a,x]+Simpl[b,x];
Simpl[a_./(x_+b_.),x_]:=Simplify[a]/(x+b) ;
Simpl[a_.,x_]:=Simplify[a]/;FreeQ[a,x]; 
Simpl[x_*a_.,x_]:=x*Simplify[a]/;FreeQ[a,x];

(*****************************************************************************)

If[FreeQ[l,xi]&&FreeQ[arg,xi],Return[G[l,arg]]];
If[ContainsAny[GasGListOfElements,{G[l,arg]}],Return[G[l,arg]/.GasGListOfReplacments]];
If[Length[l]==1,Return[RewriteGasG[G[l,arg],Variable-> xi]]];

dg=SimplifyArgOfTF[DG[G[l,arg],xi]]; (**26.03.2021***)

(*gine4s=E4SplittGCoefficients[DG[G[l,arg]/.yRep,xi]/.yInversRep,arg,RepRules,Variable-> xi,PolesLetter->apoles,EllipticCurve->yi,VAssumptions ->ass];*)
(*** this part significantly boost the calculation *********)

glist=FindListOfGs[dg];

cofglist=Table[Coefficient[dg,glist[[i]]],{i,1,Length[glist]}];

remeaningPart=dg/.Thread[glist-> 0]; (* 02.12.2021 error with remaning part fixed *******)

repForGs=Table[DeleteCases[List@@(RewriteGasG2[glist[[i]],Variable-> xi]+KastylEbat228),KastylEbat228],{i,1,Length[glist]}];

cofglistExpanded=Table[DeleteCases[List@@(Expand[GSplitt[cofglist[[i]],xi],xi]+KastylEbat228),KastylEbat228],{i,1,Length[glist]}]/.{Simpl[Sarg_,Sx_]:> Sarg};

listForInt=Flatten[Table[Flatten[Table[cofglistExpanded[[i]][[k]]*repForGs[[i]][[j]],{j,1,Length[repForGs[[i]]]},{k,1,Length[cofglistExpanded[[i]]]}]],{i,1,Length[glist]}]];


gfp=SimplifyGCoefficients[Plus @@Table[GPrimitive[listForInt[[i]],xi],{i,1,Length[listForInt]}]+GPrimitive[remeaningPart,xi],xi]; (* 02.12.2021 error with remaning part fixed *******)

(*** fixin integration constant ************)
gf=gfp-(gfp/.{xi->0});

bv=BoundaryValueGforG[G[l,arg],xi];
NLg= gf + bv; 
If[ContainsNone[GasGListOfElements,{G[l,arg]}],GasGListOfElements=Insert[GasGListOfElements,G[l,arg],-1]; GasGListOfReplacments=Insert[GasGListOfReplacments,G[l,arg]->NLg ,-1]];
NLg
]; (*Rewrites one given MPL as EMPL*)
(******************************************************************************************************************************************************************)
(******************************************************************************************************************************************************************)


(******************Transformations from G[{_},_] to IG[_,{_},_] and back *******************************************************************************************)
GsAsIGs[e_]:=e//.{G[l_,a_]:> IG[0,Table[l[[-i]],{i,1,Length[l]}],a]}; 
(*OneGIAsG[GI[a0_,al_List,an_]]:=Module[{n=Length[al],t},If[n\[Equal]1,Return[G[{al[[1]]},an]-G[{al[[1]]},a0]]];GIntegrate[OneGIAsG[GI[a0,Delete[al,-1],t]]/(t-al[[-1]]),{t,a0,an}]];*)
IGsAsGs[a_*b_]:=IGsAsGs[a]*IGsAsGs[b];
IGsAsGs[a_+b_]:=IGsAsGs[a]+IGsAsGs[b];
IGsAsGs[f_[a___]]:=IGsAsGs/@f[a];
IGsAsGs[a_]:=a/; FreeQ[a, IG];
IGsAsGs[IG[a0_,al_List,an_]]:=Module[{n=Length[al],t},If[n==1,Return[(G[{al[[1]]},an]-G[{al[[1]]},a0])//.{G[l_,0]:> 0}]];(GIntegrate[IGsAsGs[IG[a0,Delete[al,-1],t]]/(t-al[[-1]]),{t,a0,an}]//.{G[l_,0]:>  0})];
(*****************************)
(*******************************************************************************************************************************************************************)


End[]
