(* ::Package:: *)

Print["(****** Tools for EMPL Evaluation 0.5.3, Alpha ******)"];


Print["(****** Author: Maxim Bezuglov ******)"];


Print["(****** bezuglov.ma@phystech.edu ******)"];


BeginPackage["TEMPLE`"];


$ToolsEMPLEPath = Global`$TEMPLEPath;


$TEMPLECorePath = $ToolsEMPLEPath <> "/core";
$TEMPLENumericalEvaluatonG = $ToolsEMPLEPath <> "/NumericalEvaluatonG";
$CubaPath = $ToolsEMPLEPath <> "/cuba/Cuhre";


E4::usage="E4 function"


Z4; Gstar; \[Omega]1; c4; \[Omega]1; \[Omega]2; \[Eta]1; \[Eta]2;\[Lambda]e;


Block[{$Path = $TEMPLECorePath},
    Get["Defenitions.m"];
    Get["DE4.m"];
    Get["SomeParticularValuesOfE4.m"];
    Get["Integrator.m"];
    Get["Symanzik.m"];
    Get["BrownReduction.m"]
    Get["RationalizingRoots.m"];
    Get["MPLSeries.m"];    
    Get["IntegratorMPLs.m"];
    Get["Utilits.m"];
    Get["ShuffleAlgebra.m"];
    Get["EMPLSeries.m"];
    Get["MPLProperties.m"];
    Get["HopfAlgebraForMPLs.m"];
    Get["epGs.m"];
 ];


EndPackage[];
